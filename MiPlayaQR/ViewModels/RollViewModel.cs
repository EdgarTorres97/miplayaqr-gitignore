using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class RollViewModel
  {
    [Required]
    [Display(Name = "Role")]
    public string RoleName { get; set; }

  }
}
