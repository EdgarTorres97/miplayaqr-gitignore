using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Services.IServices
{
  public interface IMexicoEstadoServicio
  {
    Task<List<MexicoEstado>> ObtenerListaEstados();
  }
}
