using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class Enums
  {
    public enum Estatus
    {
      [Display(Name = "Abierto")]
      Abierto,
      [Display(Name = "Cerrado")]
      Cerrado
    }
    public enum Banderas
    {
      [Display(Name = "Verde")]
      PERMITIDO,
      [Display(Name = "Amarilla")]
      PRECAUCION,
      [Display(Name = "Roja")]
      PROHIBIDO,
      [Display(Name = "Blanca")]
      MEDUSAS,
      [Display(Name = "Negra")]
      CLAUSURADA
    }
    public enum Estacionamiento
    {
      [Display(Name = "LLeno")]
      LLENO,
      [Display(Name = "Vacio")]
      VACIO,
      [Display(Name = "Disponible")]
      DISPONIBLE
    }

    public enum Evento
    {
      [Display(Name = "Desovacion")]
      DESOVACION,
      [Display(Name = "Ninguno")]
      NINGUNO
    }
  }
}
