using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MiPlayaQR.Common.Helpers;
using MiPlayaQR.Models;
using MiPlayaQR.Services.IServices;
using MiPlayaQR.ViewModels;

namespace MiPlayaQR.Controllers
{
  public class PlayaController : Controller
  {
    private readonly IPlayaServicio _playaServicio;
    private readonly IMexicoEstadoServicio _mexicoEstadoServicio;

    public PlayaController( IPlayaServicio playaServicio, IMexicoEstadoServicio mexicoEstadoServicio)
    {
      _playaServicio = playaServicio;
      _mexicoEstadoServicio = mexicoEstadoServicio;
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Index()
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa.playas = await _playaServicio.ObtenerListPlayas();
      return View(playa);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Crear()
    {
      PlayaViewModel playa = new PlayaViewModel();
      playa.ListaMexicoEstados = await _mexicoEstadoServicio.ObtenerListaEstados();
      var listaEstados = await _mexicoEstadoServicio.ObtenerListaEstados();
      ViewBag.ListaMexicoEstados = new SelectList((from a in listaEstados
                                                   select new
                                                   {
                                                     IdEstado = a.Id,
                                                     Nombre = a.Nombre
                                                   }), "IdEstado", "Nombre");
      
      return View(playa);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Crear(Playa playa, List<IFormFile> imagen)
    {

      var response = await _playaServicio.Crear(playa, imagen);

      if (response.Success) //Validamos si el cargo se creó.
      {
        TempData["SuccessProyecto"] = response.Message;
        return RedirectToAction("Index");
      }
      else
      {


        TempData["ErrorProyecto"] = response.Message;

        return RedirectToAction("Index");
      }

    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Editar(int? id)
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPlaya(id);
      if (playa == null)
      {
        return RedirectToAction("Index");
      }
      playa.ListaMexicoEstados = await _mexicoEstadoServicio.ObtenerListaEstados();
      return View(playa);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Editar(Playa playa, List<IFormFile> imagen)
    {

      var response = await _playaServicio.Editar(playa, imagen);

      if (response.Success) //Validamos si el cargo se creó.
      {
        TempData["SuccessProyecto"] = response.Message;
        return RedirectToAction("Index");
      }
      else
      {


        TempData["ErrorProyecto"] = response.Message;

        return RedirectToAction("Index");
      }

    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Detalle(int? id)
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPlaya(id);
      if(playa == null)
      {
        return RedirectToAction("Index");
      }
      playa.ListaMexicoEstados = await _mexicoEstadoServicio.ObtenerListaEstados();
      return View(playa);
    }

    [HttpGet]
    public async Task<IActionResult> Playas(int? id)
    {
      PlayaViewModel playa = new PlayaViewModel();
      playa.playas = await _playaServicio.ObtenerListaPlayas();
      return View(playa);
    }


    //funciones
    public async Task<IActionResult> ObtenerPlayas()
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa.playas = await _playaServicio.ObtenerListPlayas();


      return Json(playa);
    }
    public async Task<IActionResult> ObtenerListaPlayasComp()
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa.playas = await _playaServicio.ObtenerListaPlayas();


      return Json(playa);
    }
    public async Task<IActionResult> ObtenerListaPlayas()
    {


      var listaPlayas = await _playaServicio.ObtenerListPlayas();


      return Json(listaPlayas);
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    public async Task<IActionResult> ObtenerListPreguntas()
    {


      var listaPreguntas = await _playaServicio.ObtenerListPreguntas();


      return Json(listaPreguntas);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    public async Task<IActionResult> ObtenerRespuestasPlaya()
    {

      var listaPlayas = await _playaServicio.ObtenerRespuestasPlaya();

      return Json(listaPlayas);
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    public async Task<IActionResult> ObtenerPlayaEditar(int? idPlaya)
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPlaya(idPlaya);


      return Json(playa);
    }

    public async Task<IActionResult> ObtenerComentariosPlaya(int? idPlaya)
    {

     var comentariosPlaya = await _playaServicio.obtenerComentarioPlaya(idPlaya);
      return Json(comentariosPlaya);
    }
    public async Task<IActionResult> ObtenerRespuestaComentarios(int? idComentario)
    {

      var respuestasComentario = await _playaServicio.obtenerRespuestasComentario(idComentario);
      return Json(respuestasComentario);
    }
    
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    public async Task<IActionResult> EliminarPlayaId(int? id)
    {
      var response = await _playaServicio.Eliminar(id);
      if (response.Success)
      {
        return Json(new { success = response.Success, msj = response.Message });
      }
      else
      {

        return Json(new { success = response.Success, msj = response.Message });
      }


    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> RespuestaEncuesta()
    {

      PlayaViewModel playa = new PlayaViewModel();

      playa.ListaPlayas = await _playaServicio.ObtenerRespuestasPlaya();

      return View(playa);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> RespuestaVotacion()
    {

      PlayaViewModel playa = new PlayaViewModel();

      playa.ListaPlayas = await _playaServicio.ObtenerVotacionesPlayas();

      return View(playa);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> EncuestaPreguntasRespuestas()
    {

      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPreguntasRespuestasPlaya();

      return View(playa);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpPost]
    public async Task<IActionResult> EncuestaPreguntasRespuestas(string pregunta, List<string> respuesta)
    {
      var response = await _playaServicio.AgregarPreguntasRespuestaPlayas(pregunta, respuesta);
      if (response.Success)
      {
        TempData["SuccessProyecto"] = response.Message;
        return RedirectToAction("Index");
      }
      else
      {
        TempData["SuccessProyecto"] = response.Message;
        
        return RedirectToAction("Index");
      }

    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    public async Task<IActionResult> ObtenerPreguntasRespuestasPlaya()
    {


      var listaPreguntas = await _playaServicio.ObtenerPreguntasRespuestasPlaya();


      return Json(listaPreguntas);
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    public async Task<IActionResult> ObtenerVotacionesPlayas()
    {


      var listaPreguntas = await _playaServicio.ObtenerVotacionesPlayas();


      return Json(listaPreguntas);
    }
  }
}
