using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiPlayaQR.Models;
using MiPlayaQR.Services.IServices;
using MiPlayaQR.ViewModels;

namespace MiPlayaQR.Controllers
{
  public class ContactoController : Controller
  {
    private readonly IContactoServicio _contactoServicio;
    public ContactoController(IContactoServicio contactoServicio)
    {
      _contactoServicio = contactoServicio;
    }
    public IActionResult Index()
    {
      return View();
    }
    [HttpPost]
    public async Task<IActionResult> Index(ContactoViewModel model)
    {
      if (ModelState.IsValid)
      {
        var response = await _contactoServicio.CrearContacto(model);
        if (response.Success)
        {
          TempData["SuccessProyecto"] = response.Message;
          return RedirectToAction("Index");
        }
        else
        {
          TempData["ErrorProyecto"] = response.Message;
        }
      }

      return View(model);
    }
    [Authorize(Roles = "Administrador,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Contactos()
    {
      ContactoViewModel contacto = new ContactoViewModel();

      contacto.Contactos = await _contactoServicio.ObtenerListaContacto();
      return View(contacto);
    }
    [HttpGet]
    public async Task<IActionResult>  DetalleContacto(int id)
    {
      Contacto contacto = new Contacto();

      contacto = await _contactoServicio.ObtenerDetalleContacto(id);
      return View(contacto);
    }
  }
}
