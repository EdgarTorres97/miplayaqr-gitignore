using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class PlayaEncuestaUsuarioViewModel
  {
    public int Id { get; set; }
    public string IdUsuario { get; set; }
    public int IdPlaya { get; set; }
    public int TotalEncuesta { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
