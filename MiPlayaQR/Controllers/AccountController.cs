using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MiPlayaQR.ViewModels;

namespace MiPlayaQR.Controllers
{
  public class AccountController : Controller
  {
    private readonly UserManager<IdentityUser> userManager;
    private readonly SignInManager<IdentityUser> signInManager;

    public AccountController(UserManager<IdentityUser> userManager,
        SignInManager<IdentityUser> signInManager)
    {
      this.userManager = userManager;
      this.signInManager = signInManager;
    }
    [HttpGet]
    [AllowAnonymous]
    public IActionResult Register()
    {
      return View();
    }
    [AcceptVerbs("Get", "Post")]
    [AllowAnonymous]
    public async Task<IActionResult> IsEmailInUse(string email)
    {
      var user = await userManager.FindByEmailAsync(email);

      if (user == null)
      {
        return Json(true);
      }
      else
      {
        return Json($"Email {email} is already in use.");
      }
    }
      [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Register(RegisterViewModel model)
    {
      if (ModelState.IsValid)
      {
        // Copy data from RegisterViewModel to IdentityUser
        var user = new IdentityUser
        {
          UserName = model.Email,
          Email = model.Email
        };

        // Store user data in AspNetUsers database table
        var result = await userManager.CreateAsync(user, model.Password);
        var result2 = await userManager.AddToRoleAsync(user,"Usuario");
        // If user is successfully created, sign-in the user using
        // SignInManager and redirect to index action of HomeController
        if (result.Succeeded && result2.Succeeded)
        {
          await signInManager.SignInAsync(user, isPersistent: false);
          return RedirectToAction("index", "home");
        }

        // If there are any errors, add them to the ModelState object
        // which will be displayed by the validation summary tag helper
        foreach (var error in result.Errors)
        {
          ModelState.AddModelError(string.Empty, error.Description);
        }
      }

      return View(model);
    }
    [HttpGet]
    [AllowAnonymous]
    public IActionResult Login()
    {
      return View();
    }


    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
    {
      if (ModelState.IsValid)
      {
        var result = await signInManager.PasswordSignInAsync(model.Email,
            model.Password, model.RememberMe, false);

        if (result.Succeeded)
        {
          if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
          {
            return Redirect(returnUrl);
          }
          else
          {
            return RedirectToAction("index", "home");
          }
        }

        ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
      }

      return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> Logout()
    {
      await signInManager.SignOutAsync();
      return RedirectToAction("index", "home");
    }

    [HttpGet]
    public async Task<IActionResult> Perfil()
    {
      var userInfo = await userManager.GetUserAsync(HttpContext.User);
      var user = await userManager.FindByIdAsync(userInfo.Id);
      if (user == null)
      {
        return RedirectToAction("Index", "Home");

      }
      var userClaims = await userManager.GetClaimsAsync(user);
      var userRoles = await userManager.GetRolesAsync(user);
      var model = new UsuarioViewModel
      {
        Id = user.Id,
        Email = user.Email,
        UserName = user.UserName,
        Claims = userClaims.Select(c => c.Value).ToList(),
        Roles = userRoles
      };
      return View(model);
      
    }

    [HttpPost]
    public async Task<IActionResult> Perfil(UsuarioViewModel model)
    {
      var userInfo = await userManager.GetUserAsync(HttpContext.User);
      var user = await userManager.FindByIdAsync(userInfo.Id);
      if(user.Id == userInfo.Id)
      {
        if (user == null)
        {
          return RedirectToAction("index", "home");
        }
        else
        {
          user.Email = model.Email;
          user.UserName = model.UserName;

          var result = await userManager.UpdateAsync(user);

          if (result.Succeeded)
          {
            return RedirectToAction("Perfil");
          }

          foreach (var error in result.Errors)
          {
            ModelState.AddModelError("", error.Description);
          }

          return View(model);
        }
      }
      else
      {
        return RedirectToAction("index", "home");
      }
      
    }
  }
}
