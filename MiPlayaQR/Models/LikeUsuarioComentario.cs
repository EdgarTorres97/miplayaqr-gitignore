using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  [Table("LikeUsuarioComentarios")]
  public class LikeUsuarioComentario
  {
    public int Id { get; set; }
    public int? IdPlaya { get; set; }
    public string IdUsuario { get; set; }
    public int? IdComentario { get; set; }
    public bool Like { get; set; }
    public bool DisLike { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
