using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  [Table("PreguntaRespuestas")]
  public class PreguntaRespuesta
  {
    public int Id { get; set; }
    [ForeignKey("Playas")]
    public int? IdPlaya { get; set; }
    [ForeignKey("Preguntas")]
    public int? IdPregunta { get; set; }
    [ForeignKey("Respuestas")]
    public int? IdRespuesta { get; set; }
    public int? Total { get; set; }
  }
}
