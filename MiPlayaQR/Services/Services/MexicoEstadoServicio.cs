using MiPlayaQR.Data;
using MiPlayaQR.Repositorys;
using MiPlayaQR.Services.IServices;
using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace MiPlayaQR.Services.Services
{
  public class MexicoEstadoServicio : IMexicoEstadoServicio
  {
    private readonly MexicoEstadoRepositorio _mexicoEstadoRepositorio;
    private readonly ILogger<PlayaServicio> _logger;
    public MexicoEstadoServicio(AppDbContext context, ILogger<PlayaServicio> logger)
    {
      _mexicoEstadoRepositorio = new MexicoEstadoRepositorio(context);
      _logger = logger;
    }

    public async Task<List<MexicoEstado>> ObtenerListaEstados()
    {
      var ListaMexicoEstados = new List<MexicoEstado>();
      try
      {
        ListaMexicoEstados = await _mexicoEstadoRepositorio.ObtenerListaEstados();
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message);
      }
      return ListaMexicoEstados;
    }
  }
}
