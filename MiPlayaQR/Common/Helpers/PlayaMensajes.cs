using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class PlayaMensajes
  {
    public readonly static string ErrorObtener = "Ha ocurrido un error al obtener la información de la playa, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorGuardar = "Ha ocurrido un error al guardar la playa, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorEliminar = "Error al intentar eliminar la playa, comuníquese con el administrador.";
    public readonly static string ErrorEditar = "Ha ocurrido un error al editar la playa, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorSubirImagen = "Ha ocurrido un error al subir la imagen.";
    // Sucess Messages
    public readonly static string CreacionExitosa = "Playa registrada con éxito.";
    public readonly static string EliminacionExitosa = "Playa eliminada con éxito.";
    public readonly static string EdicionExitosa = "Playa actualizada con éxito.";

    // Confirmation Messages
    public readonly static string ConfirmationDelete = "¿Está seguro de que desea eliminar esta playa? Al eliminar el elemento, toda su información será borrada del sistema.";
  }
}
