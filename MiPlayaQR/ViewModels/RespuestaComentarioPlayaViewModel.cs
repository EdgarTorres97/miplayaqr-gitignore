using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class RespuestaComentarioPlayaViewModel
  {
    public int Id { get; set; }
    public int? IdComentarioPlaya { get; set; }
    public string IdUsuario { get; set; }
    public IdentityUser identityUser { get; set; }
    public String ComentarioTexto { get; set; }
    public int Like { get; set; }
    public int DisLike { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
