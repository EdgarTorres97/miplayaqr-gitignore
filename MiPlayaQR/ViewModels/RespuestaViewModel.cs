using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class RespuestaViewModel
  {
    public int Id { get; set; }
    public string Descripcion { get; set; }
    public string RespuestaTexto { get; set; }
    public List<PreguntaRespuesta> PreguntaRespuestas { get; set; }
    public int PregRespId { get; set; }
    public int? Total { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
