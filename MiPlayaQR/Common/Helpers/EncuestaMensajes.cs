using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class EncuestaMensajes
  {
    public readonly static string ErrorObtener = "Ha ocurrido un error al obtener la información de la encuesta, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorGuardar = "Ha ocurrido un error al guardar la encuesta, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorRegistroEncuesta = "Registro de encuesta no valido, Usted ya ha registrado la encuesta de esta playa";
    public readonly static string ErrorEliminar = "Error al intentar eliminar la encuesta, comuníquese con el administrador.";
    public readonly static string ErrorEditar = "Ha ocurrido un error al editar la encuesta, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorSubirImagen = "Ha ocurrido un error al subir la imagen.";
    // Sucess Messages
    public readonly static string CreacionExitosa = "Encuesta registrada con éxito.";
    public readonly static string EliminacionExitosa = "Encuesta eliminada con éxito.";
    public readonly static string EdicionExitosa = "Encuesta actualizada con éxito.";

    // Confirmation Messages
    public readonly static string ConfirmationDelete = "¿Está seguro de que desea eliminar esta encuesta? Al eliminar el elemento, toda su información será borrada del sistema.";
  }
}
