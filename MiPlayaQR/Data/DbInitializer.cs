using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MiPlayaQR.Common.Helpers;


namespace MiPlayaQR.Data
{
  public class DbInitializer
  {
    public static void Initialize(AppDbContext context)
    {
      context.Database.EnsureCreated();
      DateTime today = DateTime.Today;
      if (context.MexicoEstados.Any())
      {
        return;   // DB has been seeded
      }
      var mexicoEstados = new MexicoEstado[]
      {
        new MexicoEstado{Nombre="Aguascalientes",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Baja California",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Baja California Sur",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Campeche",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Chiapas",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Chihuahua",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Coahuila",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Colima",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Distrito Federal",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Durango",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Estado de México",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Guanajuato",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Guerrero",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Hidalgo",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Jalisco",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Michoacán",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Morelos",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Nayarit",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Nuevo León",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Oaxaca",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Puebla",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Querétaro",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Quintana Roo",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="San Luis Potosí",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Sinaloa",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Sonora",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Tabasco",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Tamaulipas",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Tlaxcala",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Veracruz",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Yucatán",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new MexicoEstado{Nombre="Zacatecas",FechaCreacion=today,RowVersion=today,IsDeleted=false},
      };
      foreach (MexicoEstado m in mexicoEstados)
      {
        context.MexicoEstados.Add(m);
      }
      context.SaveChanges();
      var playas = new Playa[]
      {
        new Playa{Nombre = "Playa Tortugas",Descripcion="Ninguna descripción",IdEstado=9,Latitud="21.1516844",Longitud="-86.771683",Ubicacion="https://www.google.com.mx/maps/place/Playa+Tortugas/@21.1516844,-86.7985648,16z/data=!3m1!4b1!4m5!3m4!1s0x8f4c29315eccc1f1:0x8c7cb90332b4730d!8m2!3d21.1483155!4d-86.7908804",Estatus=0,Bandera=0,Estacionamiento=(Common.Helpers.Enums.Estacionamiento)2,Evento=(Common.Helpers.Enums.Evento)1,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new Playa{Nombre = "Playa Langosta",Descripcion="Ninguna descripción",IdEstado=9,Latitud="21.1446106",Longitud= "-86.7831526",Ubicacion="https://www.google.com.mx/maps/place/Playa+Langosta/@21.1446106,-86.7831526,17z/data=!3m1!4b1!4m5!3m4!1s0x8f4c28d86f391f4d:0x63a614d673d3c18b!8m2!3d21.1446056!4d-86.7809639",Estatus=0,Bandera=0,Estacionamiento=(Common.Helpers.Enums.Estacionamiento)2,Evento=(Common.Helpers.Enums.Evento)1,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new Playa{Nombre = "Playa Caracol",Descripcion="Ninguna descripción",IdEstado=9,Latitud="21.1383196",Longitud= "-86.7509651",Ubicacion="https://www.google.com.mx/maps/place/Playa+Caracol/@21.1383196,-86.7509651,17z/data=!3m1!4b1!4m5!3m4!1s0x8f4c28a379e256a3:0xf4293271798f3329!8m2!3d21.1391427!4d-86.7487874",Estatus=0,Bandera=0,Estacionamiento=(Common.Helpers.Enums.Estacionamiento)2,Evento=(Common.Helpers.Enums.Evento)1,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new Playa{Nombre = "Playa Chacmol",Descripcion="Ninguna descripción",IdEstado=9,Latitud="21.126933",Longitud= "-86.7526294",Ubicacion="https://www.google.com.mx/maps/place/Playa+Chacmool/@21.126933,-86.7526294,16z/data=!3m1!4b1!4m5!3m4!1s0x8f4c289a24521e33:0x91979d2042629d24!8m2!3d21.1289877!4d-86.7489292",Estatus=0,Bandera=0,Estacionamiento=(Common.Helpers.Enums.Estacionamiento)2,Evento=(Common.Helpers.Enums.Evento)1,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new Playa{Nombre = "Playa Blanca",Descripcion="Ninguna descripción",IdEstado=9,Latitud="21.1269821",Longitud= "-86.7657616",Ubicacion="https://www.google.com.mx/maps/search/playa+blanca/@21.1269821,-86.7657616,14z/data=!3m1!4b1",Estatus=0,Bandera=0,Estacionamiento=(Common.Helpers.Enums.Estacionamiento)2,Evento=(Common.Helpers.Enums.Evento)1,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new Playa{Nombre = "Playa Linda",Descripcion="Ninguna descripción",IdEstado=9,Latitud="21.1440537",Longitud= "-86.7909726",Ubicacion="https://www.google.com.mx/maps/search/playa+linda/@21.1440537,-86.7909726,17z/data=!3m1!4b1",Estatus=0,Bandera=0,Estacionamiento=(Common.Helpers.Enums.Estacionamiento)2,Evento=(Common.Helpers.Enums.Evento)1,FechaCreacion=today,RowVersion=today,IsDeleted=false}
      };
      foreach (Playa p in playas)
      {
        context.Playas.Add(p);
      }
      context.SaveChanges();
      var imagenPlaya = new ImagenPlaya[]
      {
        new ImagenPlaya{IdPlaya=1,NombreImagen="forum.jpg",URL="1_forum.jpg",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new ImagenPlaya{IdPlaya=2,NombreImagen="forum.jpg",URL="2_forum.jpg",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new ImagenPlaya{IdPlaya=3,NombreImagen="forum.jpg",URL="3_forum.jpg",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new ImagenPlaya{IdPlaya=4,NombreImagen="forum.jpg",URL="4_forum.jpg",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new ImagenPlaya{IdPlaya=5,NombreImagen="forum.jpg",URL="5_forum.jpg",FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new ImagenPlaya{IdPlaya=6,NombreImagen="forum.jpg",URL="6_forum.jpg",FechaCreacion=today,RowVersion=today,IsDeleted=false},
      };
      foreach (ImagenPlaya im in imagenPlaya)
      {
        context.ImagenPlayas.Add(im);
      }
      context.SaveChanges();

      var pregunta = new Pregunta[]
      {
        new Pregunta{PreguntaTexto="¿Qué clase de contaminante hay?"},
        new Pregunta{PreguntaTexto="¿Qué tipo de basura hay en la playa?"},
        new Pregunta{PreguntaTexto="¿Hay mucha basura?"},
        new Pregunta{PreguntaTexto="Aproximadamente ¿Cuánta gente hay?"},
      };
      foreach (Pregunta pre in pregunta)
      {
        context.Preguntas.Add(pre);
      }
      context.SaveChanges();
      var respuesta = new Respuesta[]
      {
        new Respuesta{RespuestaTexto="Generados por el hombre"},
        new Respuesta{RespuestaTexto="Generados por el sargazo"},
        new Respuesta{RespuestaTexto="No hay contaminante"},
        new Respuesta{RespuestaTexto="Hay basura organica"},
        new Respuesta{RespuestaTexto="Hay basura inorganica"},
        new Respuesta{RespuestaTexto="No hay basura"},
        new Respuesta{RespuestaTexto="Si hay mucha basura"},
        new Respuesta{RespuestaTexto="No hay mucha basura"},
        new Respuesta{RespuestaTexto="No hay basura"},
        new Respuesta{RespuestaTexto="Menos de 50 personas"},
        new Respuesta{RespuestaTexto="Mas de 100 personas"},
      };
      foreach (Respuesta resp in respuesta)
      {
        context.Respuestas.Add(resp);
      }
      context.SaveChanges();

      var playaPreguntas = new PlayaPregunta[]
      {
        new PlayaPregunta{IdPlaya=1,IdPregunta=1},
        new PlayaPregunta{IdPlaya=1,IdPregunta=2},
        new PlayaPregunta{IdPlaya=1,IdPregunta=3},
        new PlayaPregunta{IdPlaya=1,IdPregunta=4},
        new PlayaPregunta{IdPlaya=2,IdPregunta=1},
        new PlayaPregunta{IdPlaya=2,IdPregunta=2},
        new PlayaPregunta{IdPlaya=2,IdPregunta=3},
        new PlayaPregunta{IdPlaya=2,IdPregunta=4},

        new PlayaPregunta{IdPlaya=3,IdPregunta=1},
        new PlayaPregunta{IdPlaya=3,IdPregunta=2},
        new PlayaPregunta{IdPlaya=3,IdPregunta=3},
        new PlayaPregunta{IdPlaya=3,IdPregunta=4},
        new PlayaPregunta{IdPlaya=4,IdPregunta=1},
        new PlayaPregunta{IdPlaya=4,IdPregunta=2},
        new PlayaPregunta{IdPlaya=4,IdPregunta=3},
        new PlayaPregunta{IdPlaya=4,IdPregunta=4},

        new PlayaPregunta{IdPlaya=5,IdPregunta=1},
        new PlayaPregunta{IdPlaya=5,IdPregunta=2},
        new PlayaPregunta{IdPlaya=5,IdPregunta=3},
        new PlayaPregunta{IdPlaya=5,IdPregunta=4},
        new PlayaPregunta{IdPlaya=6,IdPregunta=1},
        new PlayaPregunta{IdPlaya=6,IdPregunta=2},
        new PlayaPregunta{IdPlaya=6,IdPregunta=3},
        new PlayaPregunta{IdPlaya=6,IdPregunta=4},
      };
      foreach (PlayaPregunta plaPre in playaPreguntas)
      {
        context.PlayaPreguntas.Add(plaPre);
      }
      context.SaveChanges();

      var preguntaRespuestas = new PreguntaRespuesta[]
      {
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=1,IdRespuesta=1,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=1,IdRespuesta=2,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=1,IdRespuesta=3,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=2,IdRespuesta=4,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=2,IdRespuesta=5,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=2,IdRespuesta=6,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=3,IdRespuesta=7,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=3,IdRespuesta=8,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=3,IdRespuesta=9,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=4,IdRespuesta=10,Total=0},
        new PreguntaRespuesta{IdPlaya=1,IdPregunta=4,IdRespuesta=11,Total=0},

        new PreguntaRespuesta{IdPlaya=2,IdPregunta=1,IdRespuesta=1,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=1,IdRespuesta=2,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=1,IdRespuesta=3,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=2,IdRespuesta=4,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=2,IdRespuesta=5,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=2,IdRespuesta=6,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=3,IdRespuesta=7,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=3,IdRespuesta=8,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=3,IdRespuesta=9,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=4,IdRespuesta=10,Total=0},
        new PreguntaRespuesta{IdPlaya=2,IdPregunta=4,IdRespuesta=11,Total=0},

        new PreguntaRespuesta{IdPlaya=3,IdPregunta=1,IdRespuesta=1,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=1,IdRespuesta=2,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=1,IdRespuesta=3,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=2,IdRespuesta=4,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=2,IdRespuesta=5,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=2,IdRespuesta=6,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=3,IdRespuesta=7,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=3,IdRespuesta=8,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=3,IdRespuesta=9,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=4,IdRespuesta=10,Total=0},
        new PreguntaRespuesta{IdPlaya=3,IdPregunta=4,IdRespuesta=11,Total=0},

        new PreguntaRespuesta{IdPlaya=4,IdPregunta=1,IdRespuesta=1,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=1,IdRespuesta=2,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=1,IdRespuesta=3,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=2,IdRespuesta=4,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=2,IdRespuesta=5,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=2,IdRespuesta=6,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=3,IdRespuesta=7,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=3,IdRespuesta=8,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=3,IdRespuesta=9,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=4,IdRespuesta=10,Total=0},
        new PreguntaRespuesta{IdPlaya=4,IdPregunta=4,IdRespuesta=11,Total=0},

        new PreguntaRespuesta{IdPlaya=5,IdPregunta=1,IdRespuesta=1,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=1,IdRespuesta=2,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=1,IdRespuesta=3,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=2,IdRespuesta=4,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=2,IdRespuesta=5,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=2,IdRespuesta=6,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=3,IdRespuesta=7,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=3,IdRespuesta=8,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=3,IdRespuesta=9,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=4,IdRespuesta=10,Total=0},
        new PreguntaRespuesta{IdPlaya=5,IdPregunta=4,IdRespuesta=11,Total=0},

        new PreguntaRespuesta{IdPlaya=6,IdPregunta=1,IdRespuesta=1,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=1,IdRespuesta=2,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=1,IdRespuesta=3,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=2,IdRespuesta=4,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=2,IdRespuesta=5,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=2,IdRespuesta=6,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=3,IdRespuesta=7,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=3,IdRespuesta=8,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=3,IdRespuesta=9,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=4,IdRespuesta=10,Total=0},
        new PreguntaRespuesta{IdPlaya=6,IdPregunta=4,IdRespuesta=11,Total=0},
      };
      foreach (PreguntaRespuesta preRes in preguntaRespuestas)
      {
        context.PreguntaRespuestas.Add(preRes);
      }
      var califPlaya = new CalificacionPlaya[]
      {
        new CalificacionPlaya{IdPlaya=1,VotosPositivos=0,VotosNegativos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new CalificacionPlaya{IdPlaya=2,VotosPositivos=0,VotosNegativos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new CalificacionPlaya{IdPlaya=3,VotosPositivos=0,VotosNegativos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new CalificacionPlaya{IdPlaya=4,VotosPositivos=0,VotosNegativos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new CalificacionPlaya{IdPlaya=5,VotosPositivos=0,VotosNegativos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new CalificacionPlaya{IdPlaya=6,VotosPositivos=0,VotosNegativos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false}
      };
      foreach (CalificacionPlaya calP in califPlaya)
      {
        context.CalificacionPlayas.Add(calP);
      }
      context.SaveChanges();

      var votoPlaya = new VotoPlaya[]
      {
        new VotoPlaya{IdPlaya=1,Puntuacion=0,TotalVotos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new VotoPlaya{IdPlaya=2,Puntuacion=0,TotalVotos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new VotoPlaya{IdPlaya=3,Puntuacion=0,TotalVotos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new VotoPlaya{IdPlaya=4,Puntuacion=0,TotalVotos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new VotoPlaya{IdPlaya=5,Puntuacion=0,TotalVotos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},
        new VotoPlaya{IdPlaya=6,Puntuacion=0,TotalVotos=0,FechaCreacion=today,RowVersion=today,IsDeleted=false},

      };
      foreach (VotoPlaya votoP in votoPlaya)
      {
        context.VotoPlayas.Add(votoP);
      }
      context.SaveChanges();
    }

  }
}
