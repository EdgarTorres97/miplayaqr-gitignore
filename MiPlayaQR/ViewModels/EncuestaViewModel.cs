using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class EncuestaViewModel
  {
    public int Id { get; set; }
    [ForeignKey("Playas")]
    public int? IdPlaya { get; set; }
    public string Descripcion { get; set; }
    public string Nota { get; set; }
    public double Hombre { get; set; }
    public double Sargazo { get; set; }
    public double Organica { get; set; }
    public double Inorganica { get; set; }
    public double Mucha { get; set; }
    public double Poca { get; set; }
    public double MenosCincuenta { get; set; }
    public double MasCincuenta { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }

    public PlayaViewModel playa { get; set; }
  }
}
