using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MiPlayaQR.Services.IServices
{
  interface IStandarRepositorio<TEntity> where TEntity : class
  {
    Task<List<TEntity>> ObtieneLista();
    Task<TEntity> BuscarPorId(int? id);
    Task<int> Crear(TEntity entity);
    Task<int> Actualizar(TEntity entity);
    Task<int> Eliminar(int? id);
  }
}
