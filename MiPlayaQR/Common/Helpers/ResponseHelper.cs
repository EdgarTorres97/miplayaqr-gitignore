using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class ResponseHelper
  {
    public string Message { get; set; }
    public bool Success { get; set; }
    public object HelperData { get; set; }
  }
}
