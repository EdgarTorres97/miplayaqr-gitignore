using Microsoft.EntityFrameworkCore;
using MiPlayaQR.Data;
using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Repositorys
{
  public class MexicoEstadoRepositorio
  {
    private readonly AppDbContext _context;
    public MexicoEstadoRepositorio(AppDbContext context)
    {
      _context = context;
    }
    public async Task<List<MexicoEstado>> ObtenerListaEstados()
    {
      var Estados = await (from mEst in _context.MexicoEstados
                           select new MexicoEstado
                           {
                             Id = mEst.Id,
                             Nombre = mEst.Nombre
                           }).ToListAsync();
      return Estados;
    }

  }
}
