using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  [Table("PlayaPreguntas")]
  public class PlayaPregunta
  {
    public int Id { get; set; }
    [ForeignKey("Playas")]
    public int? IdPlaya { get; set; }
    [ForeignKey("Preguntas")]
    public int? IdPregunta { get; set; }


  }
}
