using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static MiPlayaQR.Common.Helpers.Enums;

namespace MiPlayaQR.ViewModels
{
  public class PlayaViewModel
  {
    public int Id { get; set; }
    public string Nombre { get; set; }
    public int? IdEstado { get; set; }
    public MexicoEstado mexicoEstados { get; set; }
    public string Descripcion { get; set; }
    public string Imagen { get; set; }
    public string Latitud { get; set; }
    public string Longitud { get; set; }
    public string Ubicacion { get; set; }
    public Estatus Estatus { get; set; }
    public Banderas Bandera { get; set; }
    public Estacionamiento Estacionamiento { get; set; }
    public Evento Evento { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
    public VotoPlaya VotoPlayas { get; set; }
    public VotoUsuarioPlaya VotoUsuarioPlaya { get; set; }
    public ComentarioPlaya ComentarioPlaya { get; set; }
    public List<PlayaViewModel> playas { get; set; }
    public List<MexicoEstado> ListaMexicoEstados { get; set; }
    public List<ImagenPlaya> ImagenPlayas { get; set; }
    public List<PreguntaViewModel> preguntas { get; set; }
    public List<PlayaViewModel> ListaPlayas { get; set; }
    public List<Pregunta> ListaPreguntasEncuesta { get; set; }
    public List<VotoPlayaViewModel> ListaVotoPlaya { get; set; }
    public List<ComentarioPlayaViewModel> ListaComentarioPlayas { get; set; }
    public List<int> pregRespId { get; set; }
  }
}
