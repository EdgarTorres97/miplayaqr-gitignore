using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class VotaPlayaMensajes
  {
    public readonly static string ErrorObtener = "Ha ocurrido un error al obtener la información de la votación, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorGuardar = "Ha ocurrido un error al guardar la votación, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorRegistroEncuesta = "Registro de votación no valido, Usted ya ha registrado la encuesta de esta playa";
    public readonly static string ErrorEliminar = "Error al intentar eliminar la votación, comuníquese con el administrador.";
    public readonly static string ErrorEditar = "Ha ocurrido un error al editar la votación, verifique que los datos ingresados sean válidos y correctos.";
    
    // Sucess Messages
    public readonly static string CreacionExitosa = "Votación registrada con éxito.";
    public readonly static string EliminacionExitosa = "Votación eliminada con éxito.";
    public readonly static string EdicionExitosa = "Votación actualizada con éxito.";

    // Confirmation Messages
    public readonly static string ConfirmationDelete = "¿Está seguro de que desea eliminar esta Votación? Al eliminar el elemento, toda su información será borrada del sistema.";
  }
}
