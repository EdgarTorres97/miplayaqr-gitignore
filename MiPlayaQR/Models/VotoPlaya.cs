using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  [Table("VotoPlayas")]
  public class VotoPlaya
  {
    public int Id { get; set; }
    [ForeignKey("Playas")]
    public int? IdPlaya { get; set; }
    public double Puntuacion { get; set; }
    public double TotalVotos { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
