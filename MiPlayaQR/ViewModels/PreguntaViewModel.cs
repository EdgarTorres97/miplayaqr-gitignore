using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class PreguntaViewModel
  {
    public int Id { get; set; }
    public string Descripcion { get; set; }
    public string PreguntaTexto { get; set; }
    public List<RespuestaViewModel> respuesta { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
