using Microsoft.AspNetCore.Http;
using MiPlayaQR.Common.Helpers;
using MiPlayaQR.Models;
using MiPlayaQR.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Services.IServices
{
  public interface IPlayaServicio
  {
    Task<List<PlayaViewModel>> ObtenerListPlayas();
    Task<List<PlayaViewModel>> ObtenerListaPlayas();
    Task<List<PlayaViewModel>> ObtenerRespuestasPlaya();
    Task<PlayaViewModel> ObtenerPreguntasRespuestasPlaya();
    Task<ResponseHelper> AgregarPreguntasRespuestaPlayas(string pregunta, List<string> respuestas);
    Task<ResponseHelper> Encuesta(PlayaViewModel playa, string userId);
    Task<ResponseHelper> ComentarioPlaya(string comentario, string userId,int? idPlaya);
    Task<List<ComentarioPlayaViewModel>> obtenerComentarioPlaya(int? idPlaya);
    Task<List<RespuestaComentarioPlayaViewModel>> obtenerRespuestasComentario(int? idComentario);
    Task<ComentarioPlayaViewModel> AgregarLikesComentario(int? idComentario, string userId, bool opcion);
    //Task<LikeUsuarioComentario> AgregarLikeComentarioPlaya(int? idComentario);
    Task<ResponseHelper> RespuestaComentario(string RespuestaComentario, string userId, int? idComentario);
    Task<PlayaViewModel> ObtenerListPreguntas();
    Task<ResponseHelper> Crear(Playa playa, List<IFormFile> imagen);
    Task<PlayaViewModel> ObtenerPlaya(int? id);
    Task<ResponseHelper> Editar(Playa playa, List<IFormFile> imagen);
    Task<ResponseHelper> Eliminar(int? id);
    Task<ResponseHelper> CrearPlayaEncuestaUsuario(int? idPlaya, string userId);
    Task<ResponseHelper> CrearVotoPlaya(PlayaViewModel playa, string userId,double Puntuacion);
    Task<List<PlayaViewModel>> ObtenerVotacionesPlayas();
  }
}
