using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class UsuarioMensaje
  {
    public readonly static string ErrorObtener = "Ha ocurrido un error al obtener la información del Usuario, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorGuardar = "Ha ocurrido un error al guardar al Usuario, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorEliminar = "Error al intentar eliminar al Usuario, comuníquese con el administrador.";
    public readonly static string ErrorEliminarAdministrador = "No se puede eliminar al Usuario con roll de Administrador";
    public readonly static string ErrorEditar = "Ha ocurrido un error al editar al Usuario, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorSubirImagen = "Ha ocurrido un error al subir la imagen.";
    // Sucess Messages
    public readonly static string CreacionExitosa = "Usuario registrado con éxito.";
    public readonly static string EliminacionExitosa = "Usuario eliminado con éxito.";
    public readonly static string EdicionExitosa = "Usuario actualizado con éxito.";

    // Confirmation Messages
    public readonly static string ConfirmationDelete = "¿Está seguro de que desea eliminar al Usuario? Al eliminar el elemento, toda su información será borrada del sistema.";

  }
}
