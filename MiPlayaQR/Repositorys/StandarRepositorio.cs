using Microsoft.EntityFrameworkCore;
using MiPlayaQR.Data;
using MiPlayaQR.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Repositorys
{
  public class StandarRepositorio<TEntity> : IStandarRepositorio<TEntity> where TEntity : class
  {
    private readonly AppDbContext context;
    private DbSet<TEntity> entities;
    public StandarRepositorio(AppDbContext context)
    {
      this.context = context;
      this.entities = context.Set<TEntity>();
    }
    public async Task<int> Actualizar(TEntity entity)
    {
      entities.Update(entity);
      return await context.SaveChangesAsync();
    }

    public async Task<TEntity> BuscarPorId(int? id)
    {
      return await entities.FindAsync(id);
    }

    public async Task<int> Crear(TEntity entity)
    {
      await entities.AddAsync(entity);
      return await context.SaveChangesAsync();
    }

    public async Task<int> Eliminar(int? id)
    {
      var entity = await this.entities.FindAsync(id);
      entities.Remove(entity);
      return await context.SaveChangesAsync();
    }

    public async Task<List<TEntity>> ObtieneLista()
    {
      return await entities.ToListAsync();
    }
  }
}
