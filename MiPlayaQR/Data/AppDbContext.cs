using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MiPlayaQR.Data
{
  public class AppDbContext : IdentityDbContext
  {
    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }
    
    public DbSet<MexicoEstado> MexicoEstados { get; set; }
    public DbSet<Playa> Playas { get; set; }
    public DbSet<ImagenPlaya> ImagenPlayas { get; set; }
    public DbSet<Encuesta> Encuestas { get; set; }
    public DbSet<Pregunta> Preguntas { get; set; }
    public DbSet<Respuesta> Respuestas { get; set; }
    public DbSet<PlayaPregunta> PlayaPreguntas { get; set; }
    public DbSet<PreguntaRespuesta> PreguntaRespuestas { get; set; }
    public DbSet<CalificacionPlaya> CalificacionPlayas { get; set; }
    public DbSet<PlayaEncuestaUsuario> PlayaEncuestaUsuarios { get; set; }
    public DbSet<VotoPlaya> VotoPlayas { get; set; }
    public DbSet<VotoUsuarioPlaya> VotoUsuarioPlayas { get; set; }
    public DbSet<Contacto> Contactos { get; set; }
    public DbSet<ComentarioPlaya> ComentarioPlayas { get; set; }
    public DbSet<LikeUsuarioComentario> LikeUsuarioComentarios { get; set; }
    public DbSet<RespuestaComentarioPlaya> RespuestaComentarioPlayas { get; set; }
    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      // Customize the ASP.NET Identity model and override the defaults if needed.
      // For example, you can rename the ASP.NET Identity table names and more.
      // Add your customizations after calling base.OnModelCreating(builder);

      builder.Entity<MexicoEstado>().ToTable("MexicoEstados");
      builder.Entity<Playa>().ToTable("Playas");
      builder.Entity<ImagenPlaya>().ToTable("ImagenPlayas");
      builder.Entity<Encuesta>().ToTable("Encuestas");
      builder.Entity<CalificacionPlaya>().ToTable("CalificacionPlayas");
      builder.Entity<PlayaEncuestaUsuario>().ToTable("PlayaEncuestaUsuarios");
      builder.Entity<MexicoEstado>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<Playa>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<ImagenPlaya>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<Encuesta>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<CalificacionPlaya>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<PlayaEncuestaUsuario>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<Contacto>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<VotoPlaya>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<VotoUsuarioPlaya>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<ComentarioPlaya>().HasQueryFilter(p => !p.IsDeleted);
      builder.Entity<LikeUsuarioComentario>().HasQueryFilter(p => !p.IsDeleted);
    }
    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
      OnBeforeSaveChanges();
      return base.SaveChanges(acceptAllChangesOnSuccess);
    }

    public async override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
    {
      OnBeforeSaveChanges();
      var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
      return result;
    }

    private void OnBeforeSaveChanges()
    {
      try
      {
        foreach (var entry in ChangeTracker.Entries())
        {
          foreach (var property in entry.Properties)
          {
            switch (entry.State)
            {
              case EntityState.Added:
                entry.CurrentValues["IsDeleted"] = false;
                entry.CurrentValues["RowVersion"] = DateTime.Now;
                break;

              case EntityState.Deleted:
                entry.CurrentValues["IsDeleted"] = true;
                entry.CurrentValues["RowVersion"] = DateTime.Now;
                entry.State = EntityState.Modified;
                break;

              case EntityState.Modified:
                if (property.IsModified)
                {
                  entry.CurrentValues["RowVersion"] = DateTime.Now;
                }
                break;
            }
          }
        }
      }
      catch (Exception e)
      {
        Debug.WriteLine(e.Message);
      }
    }
  }
}
