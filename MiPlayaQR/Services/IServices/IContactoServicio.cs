using Microsoft.AspNetCore.Mvc;
using MiPlayaQR.Common.Helpers;
using MiPlayaQR.Models;
using MiPlayaQR.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Services.IServices
{
  public interface IContactoServicio
  {
    Task<ResponseHelper> CrearContacto(ContactoViewModel model);
    Task<List<Contacto>> ObtenerListaContacto();
    Task<Contacto> ObtenerDetalleContacto(int id);
  }
}
