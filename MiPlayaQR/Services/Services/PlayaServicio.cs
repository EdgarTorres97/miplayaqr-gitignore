using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MiPlayaQR.Common.Helpers;
using MiPlayaQR.Data;
using MiPlayaQR.Models;
using MiPlayaQR.Repositorys;
using MiPlayaQR.Services.IServices;
using MiPlayaQR.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Services.Services
{
  public class PlayaServicio : IPlayaServicio
  {
    private readonly StandarRepositorio<VotoPlaya> _standarRepository;
    private readonly StandarRepositorio<VotoUsuarioPlaya> _standarRepositoryVotoUsuarioPlaya;
    private readonly StandarRepositorio<Encuesta> _standarRepositoryEncuesta;
    private readonly StandarRepositorio<PreguntaRespuesta> _standarRepositoryPreguntaRespuesta;
    private readonly StandarRepositorio<PlayaEncuestaUsuario> _standarRepositoryPlayaEncuestaUsuario;
    private readonly StandarRepositorio<Pregunta> _standarRepositoryPlayaPregunta;
    private readonly StandarRepositorio<Respuesta> _standarRepositoryPlayaRespuesta;
    private readonly StandarRepositorio<ComentarioPlaya> _standarRepositoryComentarioPlaya;
    private readonly StandarRepositorio<RespuestaComentarioPlaya> _standarRepositoryRespuestaComentarioPlaya;
    private readonly StandarRepositorio<LikeUsuarioComentario> _standarRepositoryLikeUsuarioComentario;
    private readonly PlayaRepositorio _playaRepositorio;
    private readonly ILogger<PlayaServicio> _logger;
    public PlayaServicio(AppDbContext context, ILogger<PlayaServicio> logger)
    {
      _standarRepository = new StandarRepositorio<VotoPlaya>(context);
      _standarRepositoryVotoUsuarioPlaya = new StandarRepositorio<VotoUsuarioPlaya>(context);
      _standarRepositoryEncuesta = new StandarRepositorio<Encuesta>(context);
      _standarRepositoryPreguntaRespuesta = new StandarRepositorio<PreguntaRespuesta>(context);
      _standarRepositoryPlayaEncuestaUsuario = new StandarRepositorio<PlayaEncuestaUsuario>(context);
      _standarRepositoryPlayaPregunta = new StandarRepositorio<Pregunta>(context);
      _standarRepositoryPlayaRespuesta = new StandarRepositorio<Respuesta>(context);
      _standarRepositoryComentarioPlaya = new StandarRepositorio<ComentarioPlaya>(context);
      _standarRepositoryRespuestaComentarioPlaya = new StandarRepositorio<RespuestaComentarioPlaya>(context);
      _standarRepositoryLikeUsuarioComentario = new StandarRepositorio<LikeUsuarioComentario>(context);
      _playaRepositorio = new PlayaRepositorio(context);
      _logger = logger;
    }

    public async Task<ResponseHelper> Crear(Playa playa, List<IFormFile> imagen)
    {
      var response = new ResponseHelper();

      try
      {

        if (await _playaRepositorio.Crear(playa) > 0)
        {

          var id = playa.Id;
          string carpetaImagenes = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/assets/Playas/" + id + "/Imagenes/");
          if (!Directory.Exists(carpetaImagenes))//Valida si existe la ruta
          {
            Directory.CreateDirectory(carpetaImagenes);//Crea la ruta donde se almacenaran los adjuntos
          }
          List<ImagenPlaya> imagenes = new List<ImagenPlaya>();
          foreach (var img2 in imagen)
          {
            var name = ValidarNombre(img2.FileName);
            ImagenPlaya nuevaImagen = new ImagenPlaya
            {
              IdPlaya = id,
              NombreImagen = name,
              URL = id + "_" + name
            };
            imagenes.Add(nuevaImagen);
            var path = Path.Combine(carpetaImagenes, nuevaImagen.URL);
            using (var stream = new FileStream(path, FileMode.Create))//Añade la imagen a la ruta especificada
            {
              img2.CopyTo(stream);
            }
            await _playaRepositorio.CrearImagenPlaya(imagenes);
          }
          response.Success = true;
          response.Message = PlayaMensajes.CreacionExitosa;

        }
        else
        {
          response.Success = false;
          response.Message = PlayaMensajes.ErrorGuardar;
          _logger.LogError(response.Message);
        }
      }
      catch (Exception e)
      {

        response.Success = false;
        response.Message = PlayaMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      return response;
    }
    public string ValidarNombre(string nombre)
    {
      try
      {
        string nuevoNombre = nombre;
        string extension = Path.GetExtension(nombre);
        if (nombre.Replace(" ", string.Empty).Length > 20)//Valida si el nombre es mayor a 20 caracteres
        {
          nuevoNombre = nombre.Replace(" ", string.Empty).Remove(20) + extension;//Se cambia el nombre del archivo para que no sea tan largo
        }
        return nuevoNombre;
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message);
      }
      return nombre;
    }
    public async Task<List<PlayaViewModel>> ObtenerListPlayas()
    {
      var response = new ResponseHelper();
      var listaPlayas = new List<PlayaViewModel>();
      try
      {
        listaPlayas = await _playaRepositorio.ObtenerListPlayas();
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return listaPlayas;
    }
    public async Task<List<PlayaViewModel>> ObtenerListaPlayas()
    {
      var response = new ResponseHelper();
      var listaPlayas = new List<PlayaViewModel>();
      try
      {
        listaPlayas = await _playaRepositorio.ObtenerListaPlayas();
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return listaPlayas;
    }
    public async Task<PlayaViewModel> ObtenerPlaya(int? id)
    {
      var response = new ResponseHelper();
      var playa = new PlayaViewModel();
      try
      {
        playa = await _playaRepositorio.ObtenerPlayaEditar(id);
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return playa;
    }

    public async Task<ResponseHelper> Editar(Playa playa, List<IFormFile> imagen)
    {
      var response = new ResponseHelper();
      var id = playa.Id;


      try
      {

        if (await _playaRepositorio.Editar(playa) > 0)
        {
          var imPlaya = await _playaRepositorio.ObtenerImagenPlayaEditar(id);
          string carpetaImagenes = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/assets/Playas/" + id + "/Imagenes/");

          string carpetaEliminar = "wwwroot/assets/Playas/" + id + "/Imagenes/";
          if (Directory.Exists(carpetaImagenes))//Valida si existe la ruta
          {
            Directory.Delete(carpetaEliminar, true);
          }
          if (!Directory.Exists(carpetaImagenes))//Valida si existe la ruta
          {
            Directory.CreateDirectory(carpetaImagenes);//Crea la ruta donde se almacenaran los adjuntos
          }


          List<ImagenPlaya> imagenes = new List<ImagenPlaya>();
          foreach (var img2 in imagen)
          {
            var name = ValidarNombre(img2.FileName);
            ImagenPlaya nuevaImagen = new ImagenPlaya
            {
              Id = imPlaya.Id,
              IdPlaya = id,
              NombreImagen = name,
              URL = id + "_" + name
            };
            var path = Path.Combine(carpetaImagenes, nuevaImagen.URL);
            using (var stream = new FileStream(path, FileMode.Create))//Añade la imagen a la ruta especificada
            {
              img2.CopyTo(stream);
            }
            await _playaRepositorio.EditarImagenPlaya(nuevaImagen);
          }
          response.Success = true;
          response.Message = PlayaMensajes.CreacionExitosa;

        }
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      return response;
    }

    public async Task<ResponseHelper> Eliminar(int? id)
    {
      var response = new ResponseHelper();
      try
      {

        if (await _playaRepositorio.EliminarImagenesPlaya(id) > 0)
        {
          if (await _playaRepositorio.EliminarPlaya(id) > 0)
          {
            response.Success = true;
            response.Message = PlayaMensajes.EliminacionExitosa;
          }
          else
          {
            response.Success = false;
            response.Message = PlayaMensajes.ErrorEliminar;
            _logger.LogInformation(response.Message);
          }
        }
        else
        {
          response.Success = false;
          response.Message = PlayaMensajes.ErrorEliminar;
          _logger.LogInformation(response.Message);
        }

      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorEliminar;
        _logger.LogError(e.Message);
      }
      return response;
    }

    public async Task<List<PlayaViewModel>> ObtenerRespuestasPlaya()
    {
      var response = new ResponseHelper();
      var listaPlayas = new List<PlayaViewModel>();
      try
      {
        listaPlayas = await _playaRepositorio.ObtenerRespuestasPlaya();
      }
      catch (Exception e)
      {
        response.Success = true;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return listaPlayas;
    }
    public async Task<PlayaViewModel> ObtenerPreguntasRespuestasPlaya()
    {
      var response = new ResponseHelper();
      var listaPlayas = new PlayaViewModel();
      try
      {
        listaPlayas = await _playaRepositorio.ObtenerPreguntasRespuestasPlaya();
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return listaPlayas;
    }
    //Función que agrega una pregunta a las playas con sus posibles respuestas
    public async Task<ResponseHelper> AgregarPreguntasRespuestaPlayas(string pregunta, List<string> respuestas)
    {
      var response = new ResponseHelper();
      var contador = 0;
      DateTime today = DateTime.Now;
      try
      {
        var playas = await _playaRepositorio.ObtenerPlayas();

        if (playas != null)
        {
          var preg = new Pregunta();
          preg.PreguntaTexto = pregunta;
          preg.FechaCreacion = today;
          preg.RowVersion = today;
          if (await _playaRepositorio.AgregarPregunta(preg) < 0)
          {
            contador++;
          }
          else
          {
            foreach (var playa in playas)
            {
              var preguntaPlaya = new PlayaPregunta();
              preguntaPlaya.IdPlaya = playa.Id;
              preguntaPlaya.IdPregunta = preg.Id;
              if (await _playaRepositorio.AgregarPreguntaPlaya(preguntaPlaya) < 0)
              {
                contador++;
              }
            }
          }
          if (contador == 0)
          {
            for (int i = 0; i < respuestas.Count; i++)
            {
              if (contador == 0)
              {
                var resp = new Respuesta();
                resp.RespuestaTexto = respuestas[i];
                resp.FechaCreacion = today;
                resp.RowVersion = today;

                if (await _playaRepositorio.AgregarRespuesta(resp) < 0)
                {
                  contador++;
                }
                else
                {
                  foreach (var playa in playas)
                  {
                    var preguntaRespuesta = new PreguntaRespuesta();
                    preguntaRespuesta.IdPlaya = playa.Id;
                    preguntaRespuesta.IdPregunta = preg.Id;
                    preguntaRespuesta.IdRespuesta = resp.Id;
                    preguntaRespuesta.Total = 0;
                    if (await _playaRepositorio.AgregarPreguntaRespuesta(preguntaRespuesta) < 0)
                    {
                      contador++;
                    }
                  }
                }
              }
            }
          }
        }

        if (contador == 0)
        {
          response.Success = true;
          response.Message = EncuestaMensajes.CreacionExitosa;
        }
        else
        {
          response.Success = false;
          response.Message = EncuestaMensajes.ErrorGuardar;
          _logger.LogInformation(response.Message);
        }

      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = EncuestaMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      return response;
    }
    public async Task<PlayaViewModel> ObtenerListPreguntas()
    {
      var preguntas = new PlayaViewModel();
      try
      {
        preguntas = await _playaRepositorio.ObtenerListPreguntas();
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message);
      }
      return preguntas;
    }

    public async Task<ResponseHelper> Encuesta(PlayaViewModel playa, string userId)
    {
      var response = new ResponseHelper();
      var contador = 0;
      DateTime today = DateTime.Now;
      try
      {
        var playaEncuestaUsuarioInfo = await _playaRepositorio.ObtenerPlayaEncuestaUsuario(playa.Id, userId);
        var infoPlayaEncuesta = await _playaRepositorio.ObtenerPlayaEditar(playa.Id);
        var contadorObtener = 0;

        if (playaEncuestaUsuarioInfo != null)
        {
          var fechaUltimaEncuestaPlaya = playaEncuestaUsuarioInfo.RowVersion;

          var fechaMinima = today.AddDays(-7);
          if (fechaMinima < fechaUltimaEncuestaPlaya)
          {
            contadorObtener++;
          }
        }
        if (contadorObtener == 0)
        {


          foreach (var infoPlaya in infoPlayaEncuesta.preguntas)
          {
            foreach (var infoPla in infoPlaya.respuesta)
            {
              for (int i = 0; i < playa.pregRespId.Count; i++)
              {
                if (infoPla.PregRespId == playa.pregRespId[i])
                {
                  var total = infoPla.Total + 1;
                  var pregResp = new PreguntaRespuesta();
                  pregResp.Id = infoPla.PregRespId;
                  pregResp.IdPregunta = infoPlaya.Id;
                  pregResp.IdPlaya = playa.Id;
                  pregResp.IdRespuesta = infoPla.Id;
                  pregResp.Total = total;
                  if (await _standarRepositoryPreguntaRespuesta.Actualizar(pregResp) < 0)
                  {
                    contador++;
                  }
                }
              }
            }
          }

        }
        else
        {
          response.Success = false;
          response.Message = EncuestaMensajes.ErrorRegistroEncuesta;
          _logger.LogInformation(response.Message);
        }
        if (contador == 0)
        {
          response = await CrearPlayaEncuestaUsuario(playa.Id, userId);
        }
        else
        {
          response.Success = false;
          response.Message = EncuestaMensajes.ErrorGuardar;
          _logger.LogInformation(response.Message);

        }

      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = EncuestaMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      return response;
    }

    public async Task<ResponseHelper> CrearPlayaEncuestaUsuario(int? idPlaya, string userId)
    {
      var response = new ResponseHelper();
      DateTime today = DateTime.Now;
      try
      {
        var playaEncuestaUsuarioInfo = await _playaRepositorio.ObtenerPlayaEncuestaUsuario(idPlaya, userId);

        if (playaEncuestaUsuarioInfo != null)
        {
          var fechaUltimaEncuestaPlaya = playaEncuestaUsuarioInfo.RowVersion;


          var fechaMinima = today.AddDays(-7);
          if (fechaMinima > fechaUltimaEncuestaPlaya)
          {
            var total = playaEncuestaUsuarioInfo.TotalEncuesta + 1;
            playaEncuestaUsuarioInfo.Id = playaEncuestaUsuarioInfo.Id;
            playaEncuestaUsuarioInfo.FechaCreacion = playaEncuestaUsuarioInfo.FechaCreacion;
            playaEncuestaUsuarioInfo.RowVersion = today;
            playaEncuestaUsuarioInfo.TotalEncuesta = total;
            if (await _standarRepositoryPlayaEncuestaUsuario.Actualizar(playaEncuestaUsuarioInfo) > 0)
            {
              response.Success = true;
              response.Message = EncuestaMensajes.EdicionExitosa;
            }
            else
            {
              response.Success = false;
              response.Message = EncuestaMensajes.ErrorGuardar;
              _logger.LogInformation(response.Message);
            }
          }
          else
          {
            response.Success = false;
            response.Message = EncuestaMensajes.ErrorRegistroEncuesta;
            _logger.LogInformation(response.Message);
          }

        }
        else
        {
          var playaEncuestaUsuarioCrear = new PlayaEncuestaUsuario();
          playaEncuestaUsuarioCrear.IdPlaya = idPlaya;
          playaEncuestaUsuarioCrear.IdUsuario = userId;
          playaEncuestaUsuarioCrear.RowVersion = today;
          playaEncuestaUsuarioCrear.FechaCreacion = today;
          playaEncuestaUsuarioCrear.TotalEncuesta = 1;
          if (await _standarRepositoryPlayaEncuestaUsuario.Crear(playaEncuestaUsuarioCrear) > 0)
          {
            response.Success = true;
            response.Message = EncuestaMensajes.CreacionExitosa;

          }
          else
          {
            response.Success = false;
            response.Message = EncuestaMensajes.ErrorGuardar;
            _logger.LogInformation(response.Message);
          }
        }


      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = EncuestaMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      return response;
    }

    public async Task<ResponseHelper> CrearVotoPlaya(PlayaViewModel playa, string userId, double Puntuacion)
    {
      var response = new ResponseHelper();
      DateTime today = DateTime.Now;
      var idPlaya = playa.Id;
      var votoPlaya = playa.VotoPlayas;
      try
      {
        var votoUsuarioPlaya = await _playaRepositorio.ObtenerVotoPlayaUsuario(idPlaya, userId);
        var votosPlaya = await _playaRepositorio.ObtenerVotoPlaya(idPlaya);
        //Valido si existe algun registro de votos por el usuario en la playa seleccionada
        if (votoUsuarioPlaya != null)
        {
          var fechaUltimoVotoPlaya = votoUsuarioPlaya.RowVersion;
          var fechaMinima = today.AddDays(-7);

          //valida que no haya votado en los ultimos 7 dias
          if (fechaMinima > fechaUltimoVotoPlaya)
          {
            //Obtener la cantidad de votos que ha tenido la playa y agregarle uno, sumarle la puntuacion

            if (votosPlaya != null)
            {
              //Valores del campo a ingresar en la tabla VotoPlaya
              if (votosPlaya.Puntuacion == 0)
              {
                votosPlaya.Puntuacion = Puntuacion;
              }
              else
              {
                votosPlaya.Puntuacion = Puntuacion + votosPlaya.Puntuacion;
              }
              votosPlaya.TotalVotos = votosPlaya.TotalVotos + 1;
              if (await _standarRepository.Actualizar(votosPlaya) > 0)
              {
                var total = 0;
                if (votoUsuarioPlaya.TotalVotos == null)
                {
                  total = 1;
                }
                else
                {
                  total = (int)(votoUsuarioPlaya.TotalVotos + 1);
                }
                votoUsuarioPlaya.Id = votoUsuarioPlaya.Id;
                votoUsuarioPlaya.FechaCreacion = votoUsuarioPlaya.FechaCreacion;
                votoUsuarioPlaya.RowVersion = today;
                votoUsuarioPlaya.TotalVotos = total;
                if (await _standarRepositoryVotoUsuarioPlaya.Actualizar(votoUsuarioPlaya) > 0)
                {
                  response.Success = true;
                  response.Message = VotaPlayaMensajes.EdicionExitosa;
                }
                else
                {
                  response.Success = false;
                  response.Message = VotaPlayaMensajes.ErrorGuardar;
                  _logger.LogInformation(response.Message);
                }
              }
              //Valores del campo a ingresar en la tabla VotoUsuarioPlaya
              else
              {
                response.Success = false;
                response.Message = VotaPlayaMensajes.ErrorGuardar;
                _logger.LogInformation(response.Message);
              }
            }
            else
            {
              var votoPlayaCrear = new VotoPlaya();
              votoPlayaCrear.IdPlaya = idPlaya;
              votoPlayaCrear.Puntuacion = Puntuacion;
              votoPlayaCrear.TotalVotos = 1;
              if (await _standarRepository.Crear(votoPlayaCrear) > 0)
              {
                var total = votoUsuarioPlaya.TotalVotos + 1;
                votoUsuarioPlaya.Id = votoUsuarioPlaya.Id;
                votoUsuarioPlaya.FechaCreacion = votoUsuarioPlaya.FechaCreacion;
                votoUsuarioPlaya.RowVersion = today;
                votoUsuarioPlaya.TotalVotos = total;
                if (await _standarRepositoryVotoUsuarioPlaya.Actualizar(votoUsuarioPlaya) > 0)
                {
                  response.Success = true;
                  response.Message = VotaPlayaMensajes.EdicionExitosa;
                }
                else
                {
                  response.Success = false;
                  response.Message = VotaPlayaMensajes.ErrorGuardar;
                  _logger.LogInformation(response.Message);
                }
              }
              //Valores del campo a ingresar en la tabla VotoUsuarioPlaya
              else
              {
                response.Success = false;
                response.Message = VotaPlayaMensajes.ErrorGuardar;
                _logger.LogInformation(response.Message);
              }
            }

          }
          else
          {
            response.Success = false;
            response.Message = VotaPlayaMensajes.ErrorRegistroEncuesta;
            _logger.LogInformation(response.Message);
          }

        }
        else
        {
          //No existe VotoUsuarioPlaya y se crea
          var VotoUsuarioPlayaCrear = new VotoUsuarioPlaya();
          VotoUsuarioPlayaCrear.IdPlaya = idPlaya;
          VotoUsuarioPlayaCrear.IdUsuario = userId;
          VotoUsuarioPlayaCrear.RowVersion = today;
          VotoUsuarioPlayaCrear.FechaCreacion = today;
          VotoUsuarioPlayaCrear.TotalVotos = 1;
          if (votosPlaya != null)
          {
            if (votosPlaya.Puntuacion == 0)
            {
              votosPlaya.Puntuacion = Puntuacion;
            }
            else
            {
              votosPlaya.Puntuacion = Puntuacion + votosPlaya.Puntuacion;
            }
            //Valores del campo a ingresar en la tabla VotoPlaya

            votosPlaya.TotalVotos = votosPlaya.TotalVotos + 1;
            if (await _standarRepository.Actualizar(votosPlaya) > 0)
            {

              if (await _standarRepositoryVotoUsuarioPlaya.Crear(VotoUsuarioPlayaCrear) > 0)
              {

                response.Success = true;
                response.Message = VotaPlayaMensajes.CreacionExitosa;
              }
              else
              {
                response.Success = false;
                response.Message = VotaPlayaMensajes.ErrorGuardar;
                _logger.LogInformation(response.Message);
              }
            }
            //Valores del campo a ingresar en la tabla VotoUsuarioPlaya
            else
            {
              response.Success = false;
              response.Message = VotaPlayaMensajes.ErrorGuardar;
              _logger.LogInformation(response.Message);
            }

          }
          else
          {
            var votoPlayaCrear = new VotoPlaya();
            votoPlayaCrear.IdPlaya = idPlaya;
            votoPlayaCrear.Puntuacion = Puntuacion;
            votoPlayaCrear.TotalVotos = 1;
            if (await _standarRepository.Crear(votoPlayaCrear) > 0)
            {
              var total = 1;
              var votoUsuarioPlayaCrear = new VotoUsuarioPlaya();


              votoUsuarioPlayaCrear.FechaCreacion = today;
              votoUsuarioPlayaCrear.RowVersion = today;
              votoUsuarioPlayaCrear.TotalVotos = total;
              if (await _standarRepositoryVotoUsuarioPlaya.Crear(VotoUsuarioPlayaCrear) > 0)
              {

                response.Success = true;
                response.Message = VotaPlayaMensajes.CreacionExitosa;
              }
              else
              {
                response.Success = false;
                response.Message = VotaPlayaMensajes.ErrorGuardar;
                _logger.LogInformation(response.Message);
              }
            }
            //Valores del campo a ingresar en la tabla VotoUsuarioPlaya
            else
            {
              response.Success = false;
              response.Message = VotaPlayaMensajes.ErrorGuardar;
              _logger.LogInformation(response.Message);
            }
          }
        }

      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = VotaPlayaMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      return response;
    }

    public async Task<List<PlayaViewModel>> ObtenerVotacionesPlayas()
    {
      var response = new ResponseHelper();
      var listaPlayas = new List<PlayaViewModel>();
      try
      {
        listaPlayas = await _playaRepositorio.ObtenerRespuestasVotacionPlaya();
      }
      catch (Exception e)
      {
        response.Success = true;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return listaPlayas;
    }

    public async Task<ResponseHelper> ComentarioPlaya(string comentario, string userId, int? idPlaya)
    {
      var response = new ResponseHelper();
      DateTime today = DateTime.Now;
      try
      {

        var comentarioPlaya = new ComentarioPlaya();
        comentarioPlaya.ComentarioTexto = comentario;
        comentarioPlaya.IdPlaya = idPlaya;
        comentarioPlaya.IdUsuario = userId;
        comentarioPlaya.Like = 0;
        comentarioPlaya.DisLike = 0;
        comentarioPlaya.FechaCreacion = today;
        comentarioPlaya.RowVersion = today;

        if (await _standarRepositoryComentarioPlaya.Crear(comentarioPlaya) > 0)
        {
          response.Success = true;
          response.Message = ComentarioPlayaMensajes.CreacionExitosa;
        }
        else
        {
          response.Success = false;
          response.Message = ComentarioPlayaMensajes.ErrorGuardar;
          _logger.LogInformation(response.Message);
        }


      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = ComentarioPlayaMensajes.ErrorGuardar;
        _logger.LogInformation(response.Message);
      }
      return response;
    }

    public async Task<ResponseHelper> RespuestaComentario(string RespuestaComentario, string userId, int? idComentario)
    {
      var response = new ResponseHelper();
      DateTime today = DateTime.Now;
      try
      {

        var comentarioPlaya = new RespuestaComentarioPlaya();
        comentarioPlaya.ComentarioTexto = RespuestaComentario;
        comentarioPlaya.IdComentarioPlaya = idComentario;
        comentarioPlaya.IdUsuario = userId;
        comentarioPlaya.Like = 0;
        comentarioPlaya.DisLike = 0;
        comentarioPlaya.FechaCreacion = today;
        comentarioPlaya.RowVersion = today;

        if (await _standarRepositoryRespuestaComentarioPlaya.Crear(comentarioPlaya) > 0)
        {
          response.Success = true;
          response.Message = ComentarioPlayaMensajes.CreacionExitosa;
        }
        else
        {
          response.Success = false;
          response.Message = ComentarioPlayaMensajes.ErrorGuardar;
          _logger.LogInformation(response.Message);
        }


      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = ComentarioPlayaMensajes.ErrorGuardar;
        _logger.LogInformation(response.Message);
      }
      return response;
    }

    public async Task<List<ComentarioPlayaViewModel>> obtenerComentarioPlaya(int? idPlaya)
    {
      var response = new ResponseHelper();
      var comentarios = new List<ComentarioPlayaViewModel>();
      try
      {
        comentarios = await _playaRepositorio.ObtenerComentarioPlaya(idPlaya);
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return comentarios;
    }

    public async Task<List<RespuestaComentarioPlayaViewModel>> obtenerRespuestasComentario(int? idComentario)
    {
      var response = new ResponseHelper();
      var respuestasComentario = new List<RespuestaComentarioPlayaViewModel>();
      try
      {
        respuestasComentario = await _playaRepositorio.ObtenerRespuestasComentariosPlaya(idComentario);
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return respuestasComentario;
    }

    public async Task<ComentarioPlayaViewModel> AgregarLikesComentario(int? idComentario, string userId, bool opcion)
    {
      var response = new ResponseHelper();
      var comentario = new ComentarioPlayaViewModel();
      var comentarioConLike = new ComentarioPlaya();
      var validaExistenciaUsuarioLike = new LikeUsuarioComentario();
      var UsuarioLike = new LikeUsuarioComentario();
      DateTime today = DateTime.Now;
      var contador = 0;

      var like = true;

      var dislike = true;
      try
      {
        comentario = await _playaRepositorio.ObtenerLikesComentario(idComentario);
        validaExistenciaUsuarioLike = await _playaRepositorio.ObtenerLikesComentarioPorUsuario(idComentario, userId);
        var likes = comentario.Like;
        var dislikes = comentario.DisLike;
        if (validaExistenciaUsuarioLike != null)
        {
          if (opcion)
          {
            if (validaExistenciaUsuarioLike.Like)
            {
              validaExistenciaUsuarioLike.Like = false;
              validaExistenciaUsuarioLike.DisLike = false;
              likes = likes - 1;
              
            }
            else if(validaExistenciaUsuarioLike.DisLike)
            {
              validaExistenciaUsuarioLike.Like = true;
              validaExistenciaUsuarioLike.DisLike = false;
              likes = likes + 1;
              dislikes = dislikes -1;
            }
            else
            {
              validaExistenciaUsuarioLike.Like = true;
              validaExistenciaUsuarioLike.DisLike = false;
              likes = comentario.Like + 1;
              
            }
          }
          else if (!opcion)
          {
            if (validaExistenciaUsuarioLike.DisLike)
            {
              validaExistenciaUsuarioLike.Like = false;
              validaExistenciaUsuarioLike.DisLike = false;
              
              dislikes = dislikes- 1;
            }
            else if(validaExistenciaUsuarioLike.Like)
            {
              validaExistenciaUsuarioLike.Like = false;
              validaExistenciaUsuarioLike.DisLike = true;
              likes = likes - 1;
              dislikes = dislikes + 1;
            }
            else
            {
              validaExistenciaUsuarioLike.Like = false;
              validaExistenciaUsuarioLike.DisLike = true;
              
              dislikes = dislikes + 1;
            }
          }
          await _standarRepositoryLikeUsuarioComentario.Actualizar(validaExistenciaUsuarioLike);
        }
        else
        {
          if (opcion)
          {
            UsuarioLike.Like = true;
            UsuarioLike.DisLike = false;
            likes = likes + 1;
            
          }
          else if (!opcion)
          {
            UsuarioLike.Like = false;
            UsuarioLike.DisLike = true;
            
            dislikes = dislikes + 1;
          }
          
          UsuarioLike.IdComentario = comentario.Id;
          UsuarioLike.IdPlaya = comentario.IdPlaya;
          UsuarioLike.IdUsuario = userId;
          
          UsuarioLike.FechaCreacion = today;
          UsuarioLike.RowVersion = today;
          UsuarioLike.IsDeleted = false;
          await _standarRepositoryLikeUsuarioComentario.Crear(UsuarioLike);
          
        }

        comentarioConLike.Id = comentario.Id;
        comentarioConLike.IdPlaya = comentario.IdPlaya;
        comentarioConLike.IdUsuario = comentario.IdUsuario;
        comentarioConLike.Like = likes;
        comentarioConLike.DisLike = dislikes;
        comentarioConLike.ComentarioTexto = comentario.ComentarioTexto;
        comentarioConLike.FechaCreacion = comentario.FechaCreacion;
        comentarioConLike.RowVersion = comentario.RowVersion;
        comentarioConLike.IsDeleted = comentario.IsDeleted;
        if (await _standarRepositoryComentarioPlaya.Actualizar(comentarioConLike) > 0)
        {
          comentario = await _playaRepositorio.ObtenerLikesComentario(idComentario);
        }
        else
        {
          response.Success = false;
          response.Message = PlayaMensajes.ErrorObtener;
        }
      }
      catch (Exception e)
      {
        response.Success = false;
        response.Message = PlayaMensajes.ErrorObtener;
        _logger.LogError(e.Message);
      }
      return comentario;
    }
  }
}
