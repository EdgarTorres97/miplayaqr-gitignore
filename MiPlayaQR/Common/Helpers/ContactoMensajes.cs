using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Common.Helpers
{
  public class ContactoMensajes
  {
    public readonly static string ErrorObtener = "Ha ocurrido un error al obtener la información del contacto, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorGuardar = "Ha ocurrido un error al guardar el contacto, verifique que los datos ingresados sean válidos y correctos.";
    public readonly static string ErrorEliminar = "Error al intentar eliminar el contacto, comuníquese con el administrador.";
    public readonly static string ErrorEditar = "Ha ocurrido un error al editar el contacto, verifique que los datos ingresados sean válidos y correctos.";
    
    // Sucess Messages
    public readonly static string CreacionExitosa = "Contacto registrado con éxito.";
    public readonly static string EliminacionExitosa = "Contacto eliminado con éxito.";
    public readonly static string EdicionExitosa = "Contacto actualizado con éxito.";

    // Confirmation Messages
    public readonly static string ConfirmationDelete = "¿Está seguro de que desea eliminar este contacto? Al eliminar el elemento, toda su información será borrada del sistema.";
  }
}
