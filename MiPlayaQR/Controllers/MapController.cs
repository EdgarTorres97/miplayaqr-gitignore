using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MiPlayaQR.Services.IServices;
using MiPlayaQR.ViewModels;

namespace MiPlayaQR.Controllers
{
  public class MapController : Controller
  {
    private readonly IPlayaServicio _playaServicio;
    private readonly IMexicoEstadoServicio _mexicoEstadoServicio;
    private readonly UserManager<IdentityUser> _userManager;
    public MapController(IPlayaServicio playaServicio, IMexicoEstadoServicio mexicoEstadoServicio, UserManager<IdentityUser> userManager)
    {
      _playaServicio = playaServicio;
      _mexicoEstadoServicio = mexicoEstadoServicio;
      this._userManager = userManager;
    }

    public IActionResult Index()
    {
      return View();
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Menu(int? id)
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPlaya(id);

      return View(playa);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Menu(string Comentario,int? idPlaya)
    {
      var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var response = await _playaServicio.ComentarioPlaya(Comentario, userId,idPlaya);

      if (response.Success) //Validamos si el cargo se creó.
      {
        TempData["SuccessProyecto"] = response.Message;
        
      }
      else
      {
        TempData["ErrorProyecto"] = response.Message;

        
      }
      return RedirectToAction("Index");
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> Encuesta(int? id)
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPlaya(id);

      return View(playa);
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Encuesta(PlayaViewModel playa)
    {
      var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var response = await _playaServicio.Encuesta(playa, userId);

      if (response.Success) //Validamos si el cargo se creó.
      {
        TempData["SuccessProyecto"] = response.Message;
        return RedirectToAction("Index");
      }
      else
      {
        TempData["ErrorProyecto"] = response.Message;

        return RedirectToAction("Index");
      }

    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    [HttpGet]
    public async Task<IActionResult> VotaPlaya(int? id)
    {
      PlayaViewModel playa = new PlayaViewModel();

      playa = await _playaServicio.ObtenerPlaya(id);

      return View(playa);
    }
    [Authorize(Roles = "Administrador,Usuario,Root,Colaborador")]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> VotaPlaya(PlayaViewModel playa,double Puntuacion)
    {
      var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var response = await _playaServicio.CrearVotoPlaya(playa, userId,Puntuacion);


      if (response.Success) //Validamos si el cargo se creó.
      {
        TempData["SuccessProyecto"] = response.Message;
        return RedirectToAction("Index");
      }
      else
      {
        
        TempData["ErrorProyecto"] = response.Message;
        return RedirectToAction("Index");
      }

    }

    public async Task<IActionResult> guardarComentarioPlaya(string Comentario, int? idPlaya)
    {
      var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var response = await _playaServicio.ComentarioPlaya(Comentario, userId, idPlaya);
      if (response.Success)
      {
        return Json(new { success = response.Success, msj = response.Message });
      }
      else
      {

        return Json(new { success = response.Success, msj = response.Message });
      }


    }
    public async Task<IActionResult> guardarRespuestaComentario(string RespuestaComentario, int? idComentario)
    {
      var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var response = await _playaServicio.RespuestaComentario(RespuestaComentario, userId, idComentario);
      if (response.Success)
      {
        return Json(new { success = response.Success, msj = response.Message });
      }
      else
      {

        return Json(new { success = response.Success, msj = response.Message });
      }


    }

    public async Task<IActionResult> AgregarLikesComentario(int? idComentario, bool opcion)
    {
      var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var comentario = await _playaServicio.AgregarLikesComentario(idComentario, userId, opcion);
      return Json(comentario);
    }
  }
}
