using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  public static class ClaimsStore
  {
    public static List<Claim> AllClaims = new List<Claim>()
    {
        new Claim("Create Role", "Create Role"),
        new Claim("Edit Role","Edit Role"),
        new Claim("Delete Role","Delete Role"),
        new Claim("Create Playa", "Create Playa"),
        new Claim("Edit Playa","Edit Playa"),
        new Claim("Delete Playa","Delete Playa")
    };
  }
}
