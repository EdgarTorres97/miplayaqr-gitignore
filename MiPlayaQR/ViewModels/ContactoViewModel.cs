using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class ContactoViewModel
  {
    public int Id { get; set; }
    [Required]
    [DataType(DataType.Text)]
    [Display(Name = "Nombre Completo")]
    public string Nombre { get; set; }
    public string Telefono { get; set; }
    public string Correo { get; set; }

    public string Comentario { get; set; }
    public DateTime FechaCreacion { get; set; }
    public bool IsDeleted { get; set; }
    public List<Contacto> Contactos{get;set; }
  }
}
