using Microsoft.Extensions.Logging;
using MiPlayaQR.Common.Helpers;
using MiPlayaQR.Data;
using MiPlayaQR.Models;
using MiPlayaQR.Repositorys;
using MiPlayaQR.Services.IServices;
using MiPlayaQR.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Services.Services
{
  public class ContactoServicio: IContactoServicio
  {
    private readonly StandarRepositorio<Contacto> _standarRepository;
    private readonly ILogger<ContactoServicio> _logger;
    private readonly ContactoRepositorio _contactoRepositorio;
    public ContactoServicio( ILogger<ContactoServicio> logger, AppDbContext context)
    {
      _standarRepository = new StandarRepositorio<Contacto>(context);
      _logger = logger;
      _contactoRepositorio = new ContactoRepositorio(context);
    }

    public async Task<ResponseHelper> CrearContacto(ContactoViewModel model)
    {
      var response = new ResponseHelper();
      var telefono = "Omitido";
      var correo = "Omitido";
      DateTime today = DateTime.Now;
      if (model.Telefono != null)
      {
        telefono = model.Telefono;
      }
      if (model.Correo != null)
      {
        correo = model.Correo;
      }

      var contacto = new Contacto
      {
        Nombre = model.Nombre,
        Correo = correo,
       Telefono = telefono,
       Comentario = model.Comentario,
       FechaCreacion = today
      };
      try
      {
        if (await _standarRepository.Crear(contacto) > 0)
        {
          response.Success = true;
          response.Message = ContactoMensajes.CreacionExitosa;
        }
        else
        {
          response.Success = false;
          response.Message = ContactoMensajes.ErrorGuardar;
          _logger.LogError(response.Message);
        }
      }
      catch(Exception e)
      {
        response.Success = false;
        response.Message = ContactoMensajes.ErrorGuardar;
        _logger.LogError(e.Message);
      }
      
      return response;
    }

    public async Task<Contacto> ObtenerDetalleContacto(int id)
    {
      var Contacto = new Contacto();
      
      try
      {
        //Contacto = await _contactoRepositorio.ObtenerDetalleContacto(id);
        Contacto = await _standarRepository.BuscarPorId(id);
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message);
      }
      return Contacto;
    }

    public async Task<List<Contacto>> ObtenerListaContacto()
    {
      var ListaContacto = new List<Contacto>();
      try
      {
        ListaContacto = await _standarRepository.ObtieneLista();
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message);
      }
      return ListaContacto;
    }
  }
}
