using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class UsuarioViewModel
  {
    public UsuarioViewModel()
    {
      Claims = new List<string>();
      Roles = new List<string>();
    }
    public string Id { get; set; }
    public string NormalizedUserName { get; set; }
    [Required]
    public string UserName { get; set; }
    [Required][EmailAddress]
    public string Email { get; set; }
    public string NormalizedEmail { get; set; }
    public Boolean EmailConfirmed { get; set; }
    public string PasswordHash { get; set; }
    public string SecurityStamp { get; set; }
    public string ConcurrencyStamp { get; set; }
    public string PhoneNumber { get; set; }
    public Boolean PhoneNumberConfirmed { get; set; }
    public Boolean TwoFactorEnabled { get; set; }
    public DateTimeOffset LockoutEnd { get; set; }
    public Boolean LockoutEnabled { get; set; }
    public int AccessFailedCount { get; set; }

    public string Name { get; set; }
    public DateTime DOB { get; set; }
    public List<string> Claims { get; set; }
    public IList<string> Roles { get; set; }

    
  }
}
