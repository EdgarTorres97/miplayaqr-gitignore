using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class VotoPlayaViewModel
  {
    public int Id { get; set; }
    public int? IdPlaya { get; set; }
    public double Puntuacion { get; set; }
    public double TotalVotos { get; set; }
    public List<VotoUsuarioPlaya> VotoUsuarioPlayas { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
