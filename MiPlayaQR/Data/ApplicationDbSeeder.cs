using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Data
{
  public class ApplicationDbSeeder
  {
    private AppDbContext _context;
    private bool _seeded;

    public ApplicationDbSeeder(AppDbContext context)
    {
      // We take a dependency on the manager as we want to create a valid user
      _context = context;


    }

    public async void SeedAdminUser()
    {
      var user = new IdentityUser
      {
        UserName = "eath2497@gmail.com",
        NormalizedUserName = "EATH2497@GMAIL.COM",
        Email = "eath2497@gmail.com",
        NormalizedEmail = "EATH2497@GMAIL.COM",
        EmailConfirmed = true,
        LockoutEnabled = false,
        SecurityStamp = Guid.NewGuid().ToString()
      };
      var user2 = new IdentityUser
      {
        UserName = "antonio@gmail.com",
        NormalizedUserName = "ANTONIO@GMAIL.COM",
        Email = "antonio@gmail.com",
        NormalizedEmail = "ANTONIO@GMAIL.COM",
        EmailConfirmed = true,
        LockoutEnabled = false,
        SecurityStamp = Guid.NewGuid().ToString()
      };
      var roleStore = new RoleStore<IdentityRole>(_context);

      if (!_context.Roles.Any(r => r.Name == "Administrador"))
      {
        await roleStore.CreateAsync(new IdentityRole { Name = "Administrador", NormalizedName = "ADMINISTRADOR" });
        await roleStore.CreateAsync(new IdentityRole { Name = "Root", NormalizedName = "ROOT" });
        await roleStore.CreateAsync(new IdentityRole { Name = "Colaborador", NormalizedName = "COLABORADOR" });
        await roleStore.CreateAsync(new IdentityRole { Name = "Usuario", NormalizedName = "USUARIO" });
      }

      if (!_context.Users.Any(u => u.UserName == user.UserName))
      {
        var password = new PasswordHasher<IdentityUser>();
        var hashed = password.HashPassword(user, "Edgar_123");
        user.PasswordHash = hashed;
        var userStore = new UserStore<IdentityUser>(_context);
        await userStore.CreateAsync(user);
        await userStore.AddToRoleAsync(user, "Administrador");
      }
      if (!_context.Users.Any(u => u.UserName == user2.UserName))
      {
        var password2 = new PasswordHasher<IdentityUser>();
        var hashed2 = password2.HashPassword(user2, "Edgar_123");
        user2.PasswordHash = hashed2;
        var userStore2 = new UserStore<IdentityUser>(_context);
        await userStore2.CreateAsync(user2);
        await userStore2.AddToRoleAsync(user2, "Root");
      }
      await _context.SaveChangesAsync();
    }
  }
}

