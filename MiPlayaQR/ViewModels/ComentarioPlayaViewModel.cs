using Microsoft.AspNetCore.Identity;
using MiPlayaQR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.ViewModels
{
  public class ComentarioPlayaViewModel
  {
    public int Id { get; set; }
    public int? IdPlaya { get; set; }
    public string IdUsuario { get; set; }
    public IdentityUser identityUser { get; set; }
    public string ComentarioTexto { get; set; }
    public int Like { get; set; }
    public int DisLike { get; set; }
    public List<LikeUsuarioComentario> LikeUsuarioComentarios { get; set; }
    public List<RespuestaComentarioPlayaViewModel> ListaRespuestaComentarioPlayaViewModels { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
