using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  [Table("Contacto")]
  public class Contacto
  {
    public int Id { get; set; }
    public string Nombre { get; set; }
    public string Telefono { get; set; }
    public string Correo { get; set; }
    public string Comentario { get; set; }
    public DateTime FechaCreacion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
