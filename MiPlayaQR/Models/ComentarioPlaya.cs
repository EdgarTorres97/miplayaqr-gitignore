using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiPlayaQR.Models
{
  [Table("ComentarioPlayas")]
  public class ComentarioPlaya
  {
    public int Id { get; set; }
    public int? IdPlaya { get; set; }
    public string IdUsuario { get; set; }
    public string ComentarioTexto { get; set; }
    public int Like { get; set; }
    public int DisLike { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }
  }
}
