using Microsoft.EntityFrameworkCore;
using MiPlayaQR.Data;
using MiPlayaQR.Models;
using MiPlayaQR.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

using System.Security.Claims;
namespace MiPlayaQR.Repositorys
{
  public class PlayaRepositorio
  {
    private readonly AppDbContext _context;
    public PlayaRepositorio(AppDbContext context)
    {
      _context = context;
    }

    public async Task<List<PlayaViewModel>> ObtenerListPlayas()
    {
      var Playas = await (from playas in _context.Playas
                          join mEst in _context.MexicoEstados
                          on playas.IdEstado equals mEst.Id


                          select new PlayaViewModel
                          {
                            Id = playas.Id,
                            Nombre = playas.Nombre,
                            mexicoEstados = (from mEstd in _context.MexicoEstados

                                             where mEstd.Id == playas.IdEstado
                                             select new MexicoEstado
                                             {
                                               Id = mEstd.Id,
                                               Nombre = mEstd.Nombre
                                             }).FirstOrDefault(),
                            Descripcion = playas.Descripcion,

                            Latitud = playas.Latitud,
                            Longitud = playas.Longitud,
                            Estatus = playas.Estatus,
                            Bandera = playas.Bandera,
                            Estacionamiento = playas.Estacionamiento,
                            Evento = playas.Evento,
                            ImagenPlayas = (from imPl in _context.ImagenPlayas

                                            where playas.Id == imPl.IdPlaya
                                            select new ImagenPlaya
                                            {
                                              Id = imPl.Id,
                                              NombreImagen = imPl.NombreImagen,
                                              URL = imPl.URL
                                            }).ToList(),

                            VotoPlayas = (from votaPla in _context.VotoPlayas
                                          where votaPla.IdPlaya == playas.Id
                                          select new VotoPlaya
                                          {
                                            Id = votaPla.Id,
                                            IdPlaya = votaPla.IdPlaya,
                                            Puntuacion = votaPla.Puntuacion,
                                            TotalVotos = votaPla.TotalVotos,
                                            FechaCreacion = votaPla.FechaCreacion
                                          }).FirstOrDefault(),
                          }

                                  ).ToListAsync();

      return Playas;
    }
    public async Task<List<PlayaViewModel>> ObtenerPlayas()
    {
      var Playas = await (from playas in _context.Playas
                          join mEst in _context.MexicoEstados
                          on playas.IdEstado equals mEst.Id
                          select new PlayaViewModel
                          {
                            Id = playas.Id,
                            Nombre = playas.Nombre,
                          }
                                  ).ToListAsync();

      return Playas;
    }
    public async Task<PlayaViewModel> ObtenerListPreguntas()
    {
      var Playas = await (from playas in _context.Playas
                          join mEst in _context.MexicoEstados
                          on playas.IdEstado equals mEst.Id


                          select new PlayaViewModel
                          {
                            Id = playas.Id,
                            Nombre = playas.Nombre,
                            mexicoEstados = (from mEstd in _context.MexicoEstados

                                             where mEstd.Id == playas.IdEstado
                                             select new MexicoEstado
                                             {
                                               Id = mEstd.Id,
                                               Nombre = mEstd.Nombre
                                             }).FirstOrDefault(),
                            Descripcion = playas.Descripcion,

                            Latitud = playas.Latitud,
                            Longitud = playas.Longitud,
                            Estatus = playas.Estatus,
                            Bandera = playas.Bandera,
                            Estacionamiento = playas.Estacionamiento,
                            Evento = playas.Evento,
                            ImagenPlayas = (from imPl in _context.ImagenPlayas

                                            where playas.Id == imPl.IdPlaya
                                            select new ImagenPlaya
                                            {
                                              Id = imPl.Id,
                                              NombreImagen = imPl.NombreImagen,
                                              URL = imPl.URL
                                            }).ToList(),
                            preguntas = (from plaPre in _context.PlayaPreguntas
                                         join preg in _context.Preguntas
                                         on plaPre.IdPregunta equals preg.Id
                                         where playas.Id == plaPre.IdPlaya
                                         orderby plaPre.IdPregunta
                                         select new PreguntaViewModel
                                         {
                                           Id = preg.Id,
                                           PreguntaTexto = preg.PreguntaTexto,
                                           respuesta = (from preResp in _context.PreguntaRespuestas
                                                        join resp in _context.Respuestas
                                                        on preResp.IdRespuesta equals resp.Id
                                                        where preResp.IdPlaya == plaPre.IdPlaya && preResp.IdPregunta == plaPre.IdPregunta
                                                        orderby resp.Id
                                                        select new RespuestaViewModel
                                                        {
                                                          Id = resp.Id,
                                                          RespuestaTexto = resp.RespuestaTexto,
                                                          Total = preResp.Total

                                                        }).ToList(),
                                         }).ToList(),
                          }

                                   ).FirstOrDefaultAsync();

      return Playas;
    }

    public async Task<int> Crear(Playa playa)
    {
      var paquete = 0;
      DateTime today = DateTime.Now;
      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          await _context.Playas.AddAsync(playa);

          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            var preguntas = await ObtenerListPreguntas();
            foreach (var preg in preguntas.preguntas)
            {
              var plaPreg = new PlayaPregunta();

              plaPreg.IdPregunta = preg.Id;
              plaPreg.IdPlaya = playa.Id;
              await _context.PlayaPreguntas.AddAsync(plaPreg);

              foreach (var resp in preg.respuesta)
              {
                var respPreg = new PreguntaRespuesta();
                respPreg.IdPlaya = playa.Id;
                respPreg.IdRespuesta = resp.Id;
                respPreg.IdPregunta = preg.Id;
                respPreg.Total = 0;
                await _context.PreguntaRespuestas.AddAsync(respPreg);
              }
            }

            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }

    public async Task<List<PlayaViewModel>> ObtenerListaPlayas()
    {
      var playa = await (from playas in _context.Playas
                         select new PlayaViewModel
                         {
                           Id = playas.Id,
                           Nombre = playas.Nombre,
                           mexicoEstados = (from mEstd in _context.MexicoEstados

                                            where mEstd.Id == playas.IdEstado
                                            select mEstd).FirstOrDefault(),
                           Descripcion = playas.Descripcion,

                           Latitud = playas.Latitud,
                           Longitud = playas.Longitud,
                           Estatus = playas.Estatus,
                           Bandera = playas.Bandera,
                           Estacionamiento = playas.Estacionamiento,
                           Evento = playas.Evento,
                           ImagenPlayas = (from imPl in _context.ImagenPlayas

                                           where playas.Id == imPl.IdPlaya
                                           select imPl).ToList(),
                           preguntas = (from plaPre in _context.PlayaPreguntas
                                        join preg in _context.Preguntas
                                        on plaPre.IdPregunta equals preg.Id
                                        where playas.Id == plaPre.IdPlaya
                                        orderby plaPre.IdPregunta
                                        select new PreguntaViewModel
                                        {
                                          Id = preg.Id,
                                          PreguntaTexto = preg.PreguntaTexto,
                                          respuesta = (from preResp in _context.PreguntaRespuestas
                                                       join resp in _context.Respuestas
                                                       on preResp.IdRespuesta equals resp.Id
                                                       where preResp.IdPlaya == plaPre.IdPlaya && preResp.IdPregunta == plaPre.IdPregunta
                                                       orderby resp.Id
                                                       select new RespuestaViewModel
                                                       {
                                                         Id = resp.Id,
                                                         RespuestaTexto = resp.RespuestaTexto,
                                                         Total = preResp.Total,
                                                         PregRespId = preResp.Id

                                                       }).ToList(),
                                        }).ToList(),
                           ListaVotoPlaya = (from votaPla in _context.VotoPlayas
                                             where votaPla.IdPlaya == playas.Id
                                             select new VotoPlayaViewModel
                                             {
                                               Id = votaPla.Id,
                                               IdPlaya = votaPla.IdPlaya,
                                               Puntuacion = votaPla.Puntuacion,
                                               TotalVotos = votaPla.TotalVotos,
                                               FechaCreacion = votaPla.FechaCreacion
                                             }).ToList(),
                           ListaComentarioPlayas = (from comPla in _context.ComentarioPlayas

                                                    where comPla.IdPlaya == playas.Id
                                                    select new ComentarioPlayaViewModel
                                                    {
                                                      Id = comPla.Id,
                                                      IdPlaya = comPla.IdPlaya,
                                                      IdUsuario = comPla.IdUsuario,
                                                      identityUser = (from identity in _context.Users
                                                                      where identity.Id == comPla.IdUsuario
                                                                      select new IdentityUser
                                                                      {
                                                                        Id = identity.Id,
                                                                        NormalizedUserName = identity.NormalizedUserName,
                                                                        UserName = identity.UserName
                                                                      }).FirstOrDefault(),
                                                      ComentarioTexto = comPla.ComentarioTexto,
                                                      FechaCreacion = comPla.FechaCreacion,
                                                      Like = comPla.Like,
                                                      DisLike = comPla.DisLike,
                                                      ListaRespuestaComentarioPlayaViewModels = (from respCom in _context.RespuestaComentarioPlayas
                                                                                                 where respCom.IdComentarioPlaya == comPla.Id
                                                                                                 select new RespuestaComentarioPlayaViewModel
                                                                                                 {
                                                                                                   Id = respCom.Id,
                                                                                                   IdComentarioPlaya = respCom.Id,
                                                                                                   IdUsuario = respCom.IdUsuario,
                                                                                                   identityUser = (from identity in _context.Users
                                                                                                                   where identity.Id == respCom.IdUsuario
                                                                                                                   select new IdentityUser
                                                                                                                   {
                                                                                                                     Id = identity.Id,
                                                                                                                     NormalizedUserName = identity.NormalizedUserName,
                                                                                                                     UserName = identity.UserName
                                                                                                                   }).FirstOrDefault(),
                                                                                                   ComentarioTexto = respCom.ComentarioTexto,
                                                                                                   FechaCreacion = respCom.FechaCreacion,
                                                                                                   Like = respCom.Like,
                                                                                                   DisLike = respCom.DisLike,
                                                                                                 }).ToList(),
                                                    }).ToList(),
                         }
        ).ToListAsync();


      return playa;
    }

    public async Task<PlayaViewModel> ObtenerPlayaEditar(int? id)
    {
      var playa = await (from playas in _context.Playas

                         where playas.Id == id 
                         select new PlayaViewModel
                         {
                           Id = playas.Id,
                           Nombre = playas.Nombre,
                           mexicoEstados = (from mEstd in _context.MexicoEstados

                                            where mEstd.Id == playas.IdEstado
                                            select mEstd).FirstOrDefault(),
                           Descripcion = playas.Descripcion,

                           Latitud = playas.Latitud,
                           Longitud = playas.Longitud,
                           Estatus = playas.Estatus,
                           Bandera = playas.Bandera,
                           Estacionamiento = playas.Estacionamiento,
                           Evento = playas.Evento,
                           ImagenPlayas = (from imPl in _context.ImagenPlayas

                                           where playas.Id == imPl.IdPlaya
                                           select imPl).ToList(),
                           preguntas = (from plaPre in _context.PlayaPreguntas
                                        join preg in _context.Preguntas
                                        on plaPre.IdPregunta equals preg.Id
                                        where playas.Id == plaPre.IdPlaya
                                        orderby plaPre.IdPregunta
                                        select new PreguntaViewModel
                                        {
                                          Id = preg.Id,
                                          PreguntaTexto = preg.PreguntaTexto,
                                          respuesta = (from preResp in _context.PreguntaRespuestas
                                                       join resp in _context.Respuestas
                                                       on preResp.IdRespuesta equals resp.Id
                                                       where preResp.IdPlaya == plaPre.IdPlaya && preResp.IdPregunta == plaPre.IdPregunta
                                                       orderby resp.Id
                                                       select new RespuestaViewModel
                                                       {
                                                         Id = resp.Id,
                                                         RespuestaTexto = resp.RespuestaTexto,
                                                         Total = preResp.Total,
                                                         PregRespId = preResp.Id

                                                       }).ToList(),
                                        }).ToList(),
                           ListaVotoPlaya = (from votaPla in _context.VotoPlayas
                                             where votaPla.IdPlaya == playas.Id
                                             select new VotoPlayaViewModel
                                             {
                                               Id = votaPla.Id,
                                               IdPlaya = votaPla.IdPlaya,
                                               Puntuacion = votaPla.Puntuacion,
                                               TotalVotos = votaPla.TotalVotos,
                                               FechaCreacion = votaPla.FechaCreacion
                                             }).ToList(),
                           ListaComentarioPlayas = (from comPla in _context.ComentarioPlayas
                                                    
                                                    where comPla.IdPlaya == playas.Id
                                                    orderby comPla.Id descending
                                                    select new ComentarioPlayaViewModel
                                                    {
                                                      Id = comPla.Id,
                                                      IdPlaya = comPla.IdPlaya,
                                                      IdUsuario = comPla.IdUsuario,
                                                      identityUser = (from identity in _context.Users
                                                                      where identity.Id == comPla.IdUsuario
                                                                      select new IdentityUser
                                                                      {
                                                                        Id = identity.Id,
                                                                        NormalizedUserName = identity.NormalizedUserName,
                                                                        UserName = identity.UserName
                                                                      }).FirstOrDefault(),
                                                      ComentarioTexto = comPla.ComentarioTexto,
                                                      FechaCreacion = comPla.FechaCreacion,
                                                      Like = comPla.Like,
                                                      DisLike = comPla.DisLike,
                                                      ListaRespuestaComentarioPlayaViewModels = (from respCom in _context.RespuestaComentarioPlayas
                                                                                                 where respCom.IdComentarioPlaya == comPla.Id
                                                                                                 select new RespuestaComentarioPlayaViewModel
                                                                                                 {
                                                                                                   Id = respCom.Id,
                                                                                                   IdComentarioPlaya = respCom.Id,
                                                                                                   IdUsuario = respCom.IdUsuario,
                                                                                                   identityUser = (from identity in _context.Users
                                                                                                                   where identity.Id == respCom.IdUsuario
                                                                                                                   select new IdentityUser
                                                                                                                   {
                                                                                                                     Id = identity.Id,
                                                                                                                     NormalizedUserName = identity.NormalizedUserName,
                                                                                                                     UserName = identity.UserName
                                                                                                                   }).FirstOrDefault(),
                                                                                                   ComentarioTexto = respCom.ComentarioTexto,
                                                                                                   FechaCreacion = respCom.FechaCreacion,
                                                                                                   Like = respCom.Like,
                                                                                                   DisLike = respCom.DisLike,
                                                                                                 }).ToList(),
                                                    }).ToList(),
                         }
        ).FirstOrDefaultAsync();


      return playa;
    }
    public async Task<List<ComentarioPlayaViewModel>> ObtenerComentarioPlaya(int? id)
    {
      var ListaComentarioPlayas =  await (from comPla in _context.ComentarioPlayas

                                   where comPla.IdPlaya == id orderby comPla.Id descending
                                   select new ComentarioPlayaViewModel
                                   {
                                     Id = comPla.Id,
                                     IdPlaya = comPla.IdPlaya,
                                     IdUsuario = comPla.IdUsuario,
                                     identityUser = (from identity in _context.Users
                                                     where identity.Id == comPla.IdUsuario
                                                     select new IdentityUser
                                                     {
                                                       Id = identity.Id,
                                                       NormalizedUserName = identity.NormalizedUserName,
                                                       UserName = identity.UserName
                                                     }).FirstOrDefault(),
                                     ComentarioTexto = comPla.ComentarioTexto,
                                     FechaCreacion = comPla.FechaCreacion,
                                     Like = comPla.Like,
                                     DisLike = comPla.DisLike,
                                     ListaRespuestaComentarioPlayaViewModels = (from respCom in _context.RespuestaComentarioPlayas
                                                                                where respCom.IdComentarioPlaya == comPla.Id
                                                                                select new RespuestaComentarioPlayaViewModel
                                                                                {
                                                                                  Id = respCom.Id,
                                                                                  IdComentarioPlaya = respCom.Id,
                                                                                  IdUsuario = respCom.IdUsuario,
                                                                                  identityUser = (from identity in _context.Users
                                                                                                  where identity.Id == respCom.IdUsuario
                                                                                                  select new IdentityUser
                                                                                                  {
                                                                                                    Id = identity.Id,
                                                                                                    NormalizedUserName = identity.NormalizedUserName,
                                                                                                    UserName = identity.UserName
                                                                                                  }).FirstOrDefault(),
                                                                                  ComentarioTexto = respCom.ComentarioTexto,
                                                                                  FechaCreacion = respCom.FechaCreacion,
                                                                                  Like = respCom.Like,
                                                                                  DisLike = respCom.DisLike,
                                                                                }).ToList(),
                                   }).ToListAsync();
      return ListaComentarioPlayas;
    }
    public async Task<ComentarioPlayaViewModel> ObtenerLikesComentario(int? id)
    {
      var ListaComentarioPlayas = await (from comPla in _context.ComentarioPlayas

                                         where comPla.Id == id
                                         select new ComentarioPlayaViewModel
                                         {
                                           Id = comPla.Id,
                                           IdPlaya = comPla.IdPlaya,
                                           IdUsuario = comPla.IdUsuario,
                                           identityUser = (from identity in _context.Users
                                                           where identity.Id == comPla.IdUsuario
                                                           select new IdentityUser
                                                           {
                                                             Id = identity.Id,
                                                             NormalizedUserName = identity.NormalizedUserName,
                                                             UserName = identity.UserName
                                                           }).FirstOrDefault(),
                                           ComentarioTexto = comPla.ComentarioTexto,
                                           FechaCreacion = comPla.FechaCreacion,
                                           Like = comPla.Like,
                                           DisLike = comPla.DisLike,
                                           ListaRespuestaComentarioPlayaViewModels = (from respCom in _context.RespuestaComentarioPlayas
                                                                                      where respCom.IdComentarioPlaya == comPla.Id
                                                                                      select new RespuestaComentarioPlayaViewModel
                                                                                      {
                                                                                        Id = respCom.Id,
                                                                                        IdComentarioPlaya = respCom.Id,
                                                                                        IdUsuario = respCom.IdUsuario,
                                                                                        identityUser = (from identity in _context.Users
                                                                                                        where identity.Id == respCom.IdUsuario
                                                                                                        select new IdentityUser
                                                                                                        {
                                                                                                          Id = identity.Id,
                                                                                                          NormalizedUserName = identity.NormalizedUserName,
                                                                                                          UserName = identity.UserName
                                                                                                        }).FirstOrDefault(),
                                                                                        ComentarioTexto = respCom.ComentarioTexto,
                                                                                        FechaCreacion = respCom.FechaCreacion,
                                                                                        Like = respCom.Like,
                                                                                        DisLike = respCom.DisLike,
                                                                                      }).ToList(),
                                         }).FirstOrDefaultAsync();
      return ListaComentarioPlayas;
    }

    public async Task<LikeUsuarioComentario> ObtenerLikesComentarioPorUsuario(int? id, string idUser)
    {
      var ListaComentarioPlayas = await (from usuComLike in _context.LikeUsuarioComentarios
                                         join comPla in _context.ComentarioPlayas
                                         on  usuComLike.IdComentario equals comPla.Id
                                         where comPla.Id == id && usuComLike.IdUsuario == idUser
                                         select new LikeUsuarioComentario
                                         {
                                           Id = usuComLike.Id,
                                           IdPlaya = usuComLike.IdPlaya,
                                           IdUsuario = usuComLike.IdUsuario,
                                           IdComentario = usuComLike.IdComentario,
                                           FechaCreacion = usuComLike.FechaCreacion,
                                           Like = usuComLike.Like,
                                           DisLike = usuComLike.DisLike,
                                           
                                         }).FirstOrDefaultAsync();
      return ListaComentarioPlayas;
    }
    public async Task<List<RespuestaComentarioPlayaViewModel>> ObtenerRespuestasComentariosPlaya(int? id)
    {
      var ListaRespuestaComentarioPlayaViewModels = await (from respCom in _context.RespuestaComentarioPlayas
                                                                                      where respCom.IdComentarioPlaya == id
                                                                                      select new RespuestaComentarioPlayaViewModel
                                                                                      {
                                                                                        Id = respCom.Id,
                                                                                        IdComentarioPlaya = respCom.Id,
                                                                                        IdUsuario = respCom.IdUsuario,
                                                                                        identityUser = (from identity in _context.Users
                                                                                                        where identity.Id == respCom.IdUsuario
                                                                                                        select new IdentityUser
                                                                                                        {
                                                                                                          Id = identity.Id,
                                                                                                          NormalizedUserName = identity.NormalizedUserName,
                                                                                                          UserName = identity.UserName
                                                                                                        }).FirstOrDefault(),
                                                                                        ComentarioTexto = respCom.ComentarioTexto,
                                                                                        FechaCreacion = respCom.FechaCreacion,
                                                                                        Like = respCom.Like,
                                                                                        DisLike = respCom.DisLike,
                                                                                      
                                         }).ToListAsync();
      return ListaRespuestaComentarioPlayaViewModels;
    }
    public async Task<PlayaEncuestaUsuario> ObtenerPlayaEncuestaUsuario(int? idPlaya, string userId)
    {
      //var user = await _userManager.FindByEmailAsync(User.Identity.Name);
      var playaEncuestaUs = await (from playaEnUs in _context.PlayaEncuestaUsuarios

                                   where playaEnUs.IdUsuario == userId && playaEnUs.IdPlaya == idPlaya
                                   select new PlayaEncuestaUsuario
                                   {
                                     Id = playaEnUs.Id,
                                     IdPlaya = playaEnUs.IdPlaya,
                                     IdUsuario = playaEnUs.IdUsuario,
                                     TotalEncuesta = playaEnUs.TotalEncuesta,
                                     FechaCreacion = playaEnUs.FechaCreacion,
                                     RowVersion = playaEnUs.RowVersion

                                   }).FirstOrDefaultAsync();
      return playaEncuestaUs;
    }
    public async Task<VotoUsuarioPlaya> ObtenerVotoPlayaUsuario(int? idPlaya, string userId)
    {
      //var user = await _userManager.FindByEmailAsync(User.Identity.Name);
      var votoUsuarioP = await(from votoUsuarioPlaya in _context.VotoUsuarioPlayas

                                  where votoUsuarioPlaya.IdUsuario == userId && votoUsuarioPlaya.IdPlaya == idPlaya
                                  select new VotoUsuarioPlaya
                                  {
                                    Id = votoUsuarioPlaya.Id,
                                    IdPlaya = votoUsuarioPlaya.IdPlaya,
                                    IdUsuario = votoUsuarioPlaya.IdUsuario,
                                    TotalVotos = votoUsuarioPlaya.TotalVotos,
                                    FechaCreacion = votoUsuarioPlaya.FechaCreacion,
                                    RowVersion = votoUsuarioPlaya.RowVersion

                                  }).FirstOrDefaultAsync();
      return votoUsuarioP;
    }
    public async Task<VotoPlaya> ObtenerVotoPlaya(int? idPlaya)
    {
      //var user = await _userManager.FindByEmailAsync(User.Identity.Name);
      var votoUsuarioP = await (from votoPlaya in _context.VotoPlayas

                                where votoPlaya.IdPlaya == idPlaya
                                select new VotoPlaya
                                {
                                  Id = votoPlaya.Id,
                                  IdPlaya = votoPlaya.IdPlaya,
                                  Puntuacion = votoPlaya.Puntuacion,
                                  TotalVotos = votoPlaya.TotalVotos,
                                  FechaCreacion = votoPlaya.FechaCreacion,
                                  RowVersion = votoPlaya.RowVersion

                                }).FirstOrDefaultAsync();
      return votoUsuarioP;
    }
    
    public async Task<ImagenPlaya> ObtenerImagenPlayaEditar(int? id)
    {
      var imagen = await (from imPl in _context.ImagenPlayas

                          where imPl.IdPlaya == id
                          select new ImagenPlaya
                          {
                            Id = imPl.Id,
                            NombreImagen = imPl.NombreImagen,
                            URL = imPl.URL
                          }).FirstOrDefaultAsync();
      return imagen;
    }
    public async Task<int> Editar(Playa playa)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          _context.Playas.Update(playa);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }
    public async Task<int> EditarImagenPlaya(ImagenPlaya imPlaya)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          _context.ImagenPlayas.Update(imPlaya);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }

    public async Task<int> CrearEncuesta(int id)
    {
      var preguntasResp = await ObtenerListPreguntas();
      return id;
    }
    public async Task<int> CrearImagenPlaya(List<ImagenPlaya> imagenPlaya)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          await _context.ImagenPlayas.AddRangeAsync(imagenPlaya);

          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }

    public async Task<int> EliminarPlaya(int? IdPlaya)
    {

      var respuesta = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          var playa = await _context.Playas.FindAsync(IdPlaya);
          _context.Playas.Remove(playa);
          respuesta = await _context.SaveChangesAsync();
          if (respuesta > 0)
          {

            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          e.ToString();
        }

      }
      return respuesta;
    }

    public async Task<int> EliminarImagenesPlaya(int? IdPlaya)
    {

      var respuesta = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          var imagen = await _context.ImagenPlayas.FindAsync(IdPlaya);
          _context.ImagenPlayas.Remove(imagen);
          respuesta = await _context.SaveChangesAsync();
          if (respuesta > 0)
          {

            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          e.ToString();
        }

      }
      return respuesta;
    }

    public async Task<List<PlayaViewModel>> ObtenerRespuestasPlaya()
    {
      var Playas = await (from playas in _context.Playas
                          join mEst in _context.MexicoEstados
                          on playas.IdEstado equals mEst.Id


                          select new PlayaViewModel
                          {
                            Id = playas.Id,
                            Nombre = playas.Nombre,
                            mexicoEstados = (from mEstd in _context.MexicoEstados

                                             where mEstd.Id == playas.IdEstado
                                             select new MexicoEstado
                                             {
                                               Id = mEstd.Id,
                                               Nombre = mEstd.Nombre
                                             }).FirstOrDefault(),
                            Descripcion = playas.Descripcion,

                            Latitud = playas.Latitud,
                            Longitud = playas.Longitud,
                            Estatus = playas.Estatus,
                            Bandera = playas.Bandera,
                            Estacionamiento = playas.Estacionamiento,
                            Evento = playas.Evento,
                            ImagenPlayas = (from imPl in _context.ImagenPlayas

                                            where playas.Id == imPl.IdPlaya
                                            select new ImagenPlaya
                                            {
                                              Id = imPl.Id,
                                              NombreImagen = imPl.NombreImagen,
                                              URL = imPl.URL
                                            }).ToList(),
                            preguntas = (from plaPre in _context.PlayaPreguntas
                                         join preg in _context.Preguntas
                                         on plaPre.IdPregunta equals preg.Id
                                         where playas.Id == plaPre.IdPlaya
                                         orderby plaPre.IdPregunta
                                         select new PreguntaViewModel
                                         {
                                           Id = plaPre.Id,
                                           PreguntaTexto = preg.PreguntaTexto,
                                           respuesta = (from preResp in _context.PreguntaRespuestas
                                                        join resp in _context.Respuestas
                                                        on preResp.IdRespuesta equals resp.Id
                                                        where preResp.IdPlaya == plaPre.IdPlaya && preResp.IdPregunta == plaPre.IdPregunta
                                                        orderby resp.Id
                                                        select new RespuestaViewModel
                                                        {
                                                          Id = preResp.Id,
                                                          RespuestaTexto = resp.RespuestaTexto,
                                                          Total = preResp.Total

                                                        }).ToList(),
                                         }).ToList(),
                          }

                                  ).ToListAsync();

      return Playas;
    }
    public async Task<List<PlayaViewModel>> ObtenerRespuestasVotacionPlaya()
    {
      var Playas = await (from playas in _context.Playas
                          join mEst in _context.MexicoEstados
                          on playas.IdEstado equals mEst.Id


                          select new PlayaViewModel
                          {
                            Id = playas.Id,
                            Nombre = playas.Nombre,
                            mexicoEstados = (from mEstd in _context.MexicoEstados

                                             where mEstd.Id == playas.IdEstado
                                             select new MexicoEstado
                                             {
                                               Id = mEstd.Id,
                                               Nombre = mEstd.Nombre
                                             }).FirstOrDefault(),
                            Descripcion = playas.Descripcion,

                            Latitud = playas.Latitud,
                            Longitud = playas.Longitud,
                            Estatus = playas.Estatus,
                            Bandera = playas.Bandera,
                            Estacionamiento = playas.Estacionamiento,
                            Evento = playas.Evento,
                            ImagenPlayas = (from imPl in _context.ImagenPlayas

                                            where playas.Id == imPl.IdPlaya
                                            select new ImagenPlaya
                                            {
                                              Id = imPl.Id,
                                              NombreImagen = imPl.NombreImagen,
                                              URL = imPl.URL
                                            }).ToList(),
                            ListaVotoPlaya = (from votaPla in _context.VotoPlayas
                                              where votaPla.IdPlaya == playas.Id
                                              select new VotoPlayaViewModel
                                              {
                                                Id = votaPla.Id,
                                                IdPlaya = votaPla.IdPlaya,
                                                Puntuacion = votaPla.Puntuacion,
                                                TotalVotos = votaPla.TotalVotos,
                                                FechaCreacion = votaPla.FechaCreacion
                                              }).ToList(),
                          }).ToListAsync();

      return Playas;
    }
    //Funcion para obtener la lista de preguntas y respuestas 
    public async Task<PlayaViewModel> ObtenerPreguntasRespuestasPlaya()
    {
      var Playas = await (from playas in _context.Playas
                          select new PlayaViewModel
                          {
                            preguntas = (from plaPre in _context.PlayaPreguntas
                                         join preg in _context.Preguntas
                                         on plaPre.IdPregunta equals preg.Id
                                         where playas.Id == plaPre.IdPlaya
                                         orderby plaPre.IdPregunta
                                         select new PreguntaViewModel
                                         {
                                           Id = plaPre.Id,
                                           PreguntaTexto = preg.PreguntaTexto,
                                           respuesta = (from preResp in _context.PreguntaRespuestas
                                                        join resp in _context.Respuestas
                                                        on preResp.IdRespuesta equals resp.Id
                                                        where preResp.IdPlaya == plaPre.IdPlaya && preResp.IdPregunta == plaPre.IdPregunta
                                                        orderby resp.Id
                                                        select new RespuestaViewModel
                                                        {
                                                          Id = preResp.Id,
                                                          RespuestaTexto = resp.RespuestaTexto,
                                                          Total = preResp.Total

                                                        }).ToList(),
                                         }).ToList(),
                          }

                                  ).FirstOrDefaultAsync();

      return Playas;
    }

    public async Task<int> PlayaPreguntaAgregar(PlayaPregunta playa)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          _context.PlayaPreguntas.Update(playa);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }
    public async Task<int> AgregarPregunta(Pregunta pregunta)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {

          _context.Preguntas.Update(pregunta);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }
    public async Task<int> AgregarRespuesta(Respuesta respuesta)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {

          _context.Respuestas.Update(respuesta);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }
    public async Task<int> AgregarPreguntaPlaya(PlayaPregunta playaPregunta)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {

          _context.PlayaPreguntas.Update(playaPregunta);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }
    public async Task<int> AgregarPreguntaRespuesta(PreguntaRespuesta preguntaRespuesta)
    {
      var paquete = 0;

      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {

          _context.PreguntaRespuestas.Update(preguntaRespuesta);
          paquete = await _context.SaveChangesAsync();
          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }

    //Comentarios
    public async Task<int> CrearComentarioPlaya(ComentarioPlaya comentarioPlaya)
    {
      var paquete = 0;
      DateTime today = DateTime.Now;
      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          await _context.ComentarioPlayas.AddAsync(comentarioPlaya);

          paquete = await _context.SaveChangesAsync();

          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }

    public async Task<int> CrearLikeUsuarioComentario(LikeUsuarioComentario likeUsuarioComentario)
    {
      var paquete = 0;
      DateTime today = DateTime.Now;
      using (var transacion = _context.Database.BeginTransaction())
      {
        try
        {
          await _context.LikeUsuarioComentarios.AddAsync(likeUsuarioComentario);

          paquete = await _context.SaveChangesAsync();

          if (paquete > 0)
          {
            transacion.Commit();
          }
          else
          {
            transacion.Rollback();
          }
        }
        catch (Exception e)
        {
          transacion.Rollback();
          paquete = 0;
          e.ToString();
        }
      }
      return paquete;
    }
  }
}
