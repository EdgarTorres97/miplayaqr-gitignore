using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MiPlayaQR.Common.Helpers;
using MiPlayaQR.Models;
using MiPlayaQR.ViewModels;

namespace MiPlayaQR.Controllers
{
  [Authorize(Roles = "Administrador,Root,Colaborador")]
  public class AdministradorController : Controller
  {
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly UserManager<IdentityUser> _userManager;
    public AdministradorController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
    {
      this._roleManager = roleManager;
      this._userManager = userManager;
    }
    [HttpGet]
    
    public IActionResult CrearRoll()
    {
      return View();
    }

    [HttpPost]
    public async Task<IActionResult> CrearRoll(RollViewModel model)
    {
      if (ModelState.IsValid)
      {
        // We just need to specify a unique role name to create a new role
        IdentityRole identityRole = new IdentityRole
        {
          Name = model.RoleName
        };

        // Saves the role in the underlying AspNetRoles table
        IdentityResult result = await _roleManager.CreateAsync(identityRole);

        if (result.Succeeded)
        {
          return RedirectToAction("index", "home");
        }

        foreach (IdentityError error in result.Errors)
        {
          ModelState.AddModelError("", error.Description);
        }
      }

      return View(model);
    }
    
    [HttpGet]
    public IActionResult Roles()
    {
      var roles = _roleManager.Roles;
      return View(roles);
    }
    [HttpGet]
    public async Task<IActionResult> EditarRoll(string id)
    {
      // Find the role by Role ID
      var role = await _roleManager.FindByIdAsync(id);

      if (role == null)
      {
        ViewBag.ErrorMessage = $"Role with Id = {id} cannot be found";
        return View("NotFound");
      }

      var model = new EditarRolViewModel
      {
        Id = role.Id,
        RoleName = role.Name
      };

      // Retrieve all the Users
      foreach (var user in _userManager.Users)
      {
        // If the user is in this role, add the username to
        // Users property of EditRoleViewModel. This model
        // object is then passed to the view for display
        if (await _userManager.IsInRoleAsync(user, role.Name))
        {
          model.Users.Add(user.UserName);
        }
      }

      return View(model);
    }

    // This action responds to HttpPost and receives EditRoleViewModel
    [HttpPost]
    public async Task<IActionResult> EditarRoll(EditarRolViewModel model)
    {
      var role = await _roleManager.FindByIdAsync(model.Id);

      if (role == null)
      {
        ViewBag.ErrorMessage = $"Role with Id = {model.Id} cannot be found";
        return View("NotFound");
      }
      else
      {
        role.Name = model.RoleName;

        // Update the Role using UpdateAsync
        var result = await _roleManager.UpdateAsync(role);

        if (result.Succeeded)
        {
          return RedirectToAction("Roles");
        }

        foreach (var error in result.Errors)
        {
          ModelState.AddModelError("", error.Description);
        }

        return View(model);
      }
    }
    [HttpGet]
    public IActionResult CrearUsuario()
    {
      RegistroUsuarioViewModel usuario = new RegistroUsuarioViewModel();
      usuario.roles = _roleManager.Roles;

      return View(usuario);
    }
    [HttpPost]
    public async Task<IActionResult> CrearUsuario(RegistroUsuarioViewModel model)
    {
      if (ModelState.IsValid)
      {
        // We just need to specify a unique role name to create a new role
        var user = new IdentityUser
        {
          UserName = model.Email,
          Email = model.Email
        };
        var result = await _userManager.CreateAsync(user, model.Password);
        var result2 = await _userManager.AddToRoleAsync(user, model.rol);
        if (result.Succeeded && result2.Succeeded)
        {
          return RedirectToAction("index", "home");
        }

        foreach (IdentityError error in result.Errors)
        {
          ModelState.AddModelError("", error.Description);
        }
      }

      return View(model);
    }
    [HttpGet]
    public async Task<IActionResult> EditarUsuarioRoll(string roleId)
    {
      ViewBag.roleId = roleId;

      var role = await _roleManager.FindByIdAsync(roleId);

      if (role == null)
      {
        ViewBag.ErrorMessage = $"Role with Id = {roleId} cannot be found";
        return View("NotFound");
      }

      var model = new List<UsuarioRollViewModel>();

      foreach (var user in _userManager.Users)
      {
        var userRoleViewModel = new UsuarioRollViewModel
        {
          UserId = user.Id,
          UserName = user.UserName
        };

        if (await _userManager.IsInRoleAsync(user, role.Name))
        {
          userRoleViewModel.IsSelected = true;
        }
        else
        {
          userRoleViewModel.IsSelected = false;
        }

        model.Add(userRoleViewModel);
      }

      return View(model);
    }


    [HttpPost]
    public async Task<IActionResult> EditarUsuarioRoll(List<UsuarioRollViewModel> model, string roleId)
    {
      var role = await _roleManager.FindByIdAsync(roleId);

      if (role == null)
      {
        ViewBag.ErrorMessage = $"Role with Id = {roleId} cannot be found";
        return View("NotFound");
      }

      for (int i = 0; i < model.Count; i++)
      {
        var user = await _userManager.FindByIdAsync(model[i].UserId);

        IdentityResult result = null;

        if (model[i].IsSelected && !(await _userManager.IsInRoleAsync(user, role.Name)))
        {
          result = await _userManager.AddToRoleAsync(user, role.Name);
        }
        else if (!model[i].IsSelected && await _userManager.IsInRoleAsync(user, role.Name))
        {
          result = await _userManager.RemoveFromRoleAsync(user, role.Name);
        }
        else
        {
          continue;
        }

        if (result.Succeeded)
        {
          if (i < (model.Count - 1))
            continue;
          else
            return RedirectToAction("EditarRoll", new { Id = roleId });
        }
      }

      return RedirectToAction("EditarRoll", new { Id = roleId });
    }

    [HttpGet]
    public IActionResult Usuarios()
    {
      var users = _userManager.Users;
      return View(users);
    }



    [HttpGet]
    public async Task<IActionResult> EditarUsuario(string id)
    {
      var user = await _userManager.FindByIdAsync(id);
      if (user == null)
      {
        ViewBag.ErrorMessage = $"User with Id={id} cannot be found";
        return View("NotFound");
      }
      var userClaims = await _userManager.GetClaimsAsync(user);
      var userRoles = await _userManager.GetRolesAsync(user);
      var model = new UsuarioViewModel
      {
        Id = user.Id,
        Email = user.Email,
        UserName = user.UserName,
        Claims = userClaims.Select(c => c.Value).ToList(),
        Roles = userRoles
      };
      return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> EditarUsuario(UsuarioViewModel model)
    {
      var user = await _userManager.FindByIdAsync(model.Id);

      if (user == null)
      {
        ViewBag.ErrorMessage = $"User with Id = {model.Id} cannot be found";
        return View("NotFound");
      }
      else
      {
        user.Email = model.Email;
        user.UserName = model.UserName;

        var result = await _userManager.UpdateAsync(user);

        if (result.Succeeded)
        {
          return RedirectToAction("Usuarios");
        }

        foreach (var error in result.Errors)
        {
          ModelState.AddModelError("", error.Description);
        }

        return View(model);
      }
    }
    public async Task<IActionResult> EliminarUsuario(string id)
    {
      var user = await _userManager.FindByIdAsync(id);
      if (user == null)
      {
        return Json(new { success = $"User with Id = {id} cannot be found", msj = $"User with Id = {id} cannot be found" });
      }
      else
      {
        var userClaims = await _userManager.GetClaimsAsync(user);
        var userRoles = await _userManager.GetRolesAsync(user);
        var contador = 0;
        var model = new UsuarioViewModel
        {
          Id = user.Id,
          Email = user.Email,
          UserName = user.UserName,
          Claims = userClaims.Select(c => c.Value).ToList(),
          Roles = userRoles
        };
        var response = new ResponseHelper();
        for(int i = 0; i < userRoles.Count; i++)
        {
          if(contador == 0)
          {
            if (userRoles[i] == "Administrador")
            {
              contador++;
            }
          }
          
        }
        if(contador == 0)
        {
          var result = await _userManager.DeleteAsync(user);

          if (result.Succeeded)
          {

            response.Success = true;
            response.Message = UsuarioMensaje.EliminacionExitosa;
            return Json(new { success = response.Success, msj = response.Message });
          }
          else
          {
            response.Success = false;
            response.Message = UsuarioMensaje.ErrorEliminar;
            return Json(new { success = response.Success, msj = response.Message });
          }
        }
        else
        {
          response.Success = false;
          response.Message = UsuarioMensaje.ErrorEliminarAdministrador;
          
          return Json(new { success = response.Message, msj = "warning" });
        }
      }
    }
    /*
    [HttpPost]
    [Authorize(Policy = "DeleteRolePolicy")]
    public async Task<IActionResult> EliminarRoll(string id)
    {
      var user = await _userManager.FindByIdAsync(id);
      
      var role = await _roleManager.FindByIdAsync(id);
      if (role == null)
      {

        return Json(new { success = $"Role with Id = {id} cannot be found", msj = $"User with Id = {id} cannot be found" });
      }
      else
      {
        var result = await _roleManager.DeleteAsync(role);
        var response = new ResponseHelper();
        if (result.Succeeded)
        {

          response.Success = true;
          response.Message = UsuarioMensaje.EliminacionExitosa;
          return Json(new { success = response.Success, msj = response.Message });
        }
        else
        {
          response.Success = false;
          response.Message = UsuarioMensaje.ErrorEliminar;
          return Json(new { success = response.Success, msj = response.Message });
        }

      }

    }
    */
    [HttpGet]
    public async Task<IActionResult> ManageUserClaims(string userId)
    {
      var user = await _userManager.FindByIdAsync(userId);

      if (user == null)
      {
        ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
        return View("NotFound");
      }

      // UserManager service GetClaimsAsync method gets all the current claims of the user
      var existingUserClaims = await _userManager.GetClaimsAsync(user);

      var model = new UserClaimsViewModel
      {
        UserId = userId
      };

      // Loop through each claim we have in our application
      foreach (Claim claim in ClaimsStore.AllClaims)
      {
        UserClaim userClaim = new UserClaim
        {
          ClaimType = claim.Type
        };

        // If the user has the claim, set IsSelected property to true, so the checkbox
        // next to the claim is checked on the UI
        if (existingUserClaims.Any(c => c.Type == claim.Type))
        {
          userClaim.IsSelected = true;
        }

        model.Cliams.Add(userClaim);
      }

      return View(model);

    }

    [HttpPost]
    public async Task<IActionResult> ManageUserClaims(UserClaimsViewModel model)
    {
      var user = await _userManager.FindByIdAsync(model.UserId);

      if (user == null)
      {
        ViewBag.ErrorMessage = $"User with Id = {model.UserId} cannot be found";
        return View("NotFound");
      }

      // Get all the user existing claims and delete them
      var claims = await _userManager.GetClaimsAsync(user);
      var result = await _userManager.RemoveClaimsAsync(user, claims);

      if (!result.Succeeded)
      {
        ModelState.AddModelError("", "Cannot remove user existing claims");
        return View(model);
      }

      // Add all the claims that are selected on the UI
      result = await _userManager.AddClaimsAsync(user,
          model.Cliams.Where(c => c.IsSelected).Select(c => new Claim(c.ClaimType, c.ClaimType)));

      if (!result.Succeeded)
      {
        ModelState.AddModelError("", "Cannot add selected claims to user");
        return View(model);
      }

      return RedirectToAction("EditarUsuario", new { Id = model.UserId });

    }

    [HttpGet]
    [AllowAnonymous]
    public IActionResult AccessDenied()
    {
      return View();
    }

  }
}
