using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using static MiPlayaQR.Common.Helpers.Enums;

namespace MiPlayaQR.Models
{
  [Table("Playas")]
  public class Playa
  {
    public int Id { get; set; }
    public string Nombre { get; set; }
    [ForeignKey("EstadosMexico")]
    public int? IdEstado { get; set; }
    public string Descripcion { get; set; }
    public string Latitud { get; set; }
    public string Longitud { get; set; }
    public string Ubicacion { get; set; }
    public Estatus Estatus { get; set; }
    public Banderas Bandera { get; set; }
    public Estacionamiento Estacionamiento { get; set; }
    public Evento Evento { get; set; }
    public DateTime FechaCreacion { get; set; }
    public DateTime RowVersion { get; set; }
    public bool IsDeleted { get; set; }


  }
}
